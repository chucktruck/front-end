(function () {


  $.CT.HomeView = function () {

    var _$selectedBoxEl,

      _contactUsFormValidator,

      _scrollPosition,

      _domEl = {
        $alertContainer: null,
        $body: null,
        $homeContainer: null,
        $boxes: null,
        $contactForm: null,
        $contactUsSubmitButton: null,
        $navbarHideButton: null,
        $navbarMenu: null,
        $navbarToggleButton: null,
        $ourApproachWrapper: null,
        $removeSelectedBox: null,
        $services: null,
        $window: null
      },

      ALERT_TYPE = {
        ERROR: 'danger',
        SUCCESS: 'success'
      },

      CLASS = {
        BOX: 'box',
        CAROUSEL_MODE: 'carousel-mode',
        ERROR: 'has-error',
        IN: 'in',
        READY: 'ready',
        SELECTED: 'selected',
        SHOW: 'show'
      },

      ERRORS = {
        COMMENTS_REQUIRED: 'Please enter message',
        COMMENTS_SIZE: 'Please enter more details into message',
        EMAIL_REQUIRED: 'Please enter your email',
        EMAIL_TYPE: 'Please enter a valid email',
        GENERIC: 'Please fix the errors below',
        NAME_REQUIRED: 'Please enter your name',
        NAME_SIZE: 'Please enter a valid name'
      },

      EVENT = {
        CLICK: 'click',
        KEYUP: 'keyup',
        SCROLL: 'scroll'
      },

      PROP = {
        DISABLED: 'disabled'
      },

      SEL = {
        ALERT_CONTAINER: '.alert-container',
        BODY: 'body',
        BOX: '.box',
        BOXES: '.boxes',
        COMMENTS: "#comments",
        CONTACT_FORM: '#contact-form',
        CONTACT_US_SUBMIT_BTN: '#contact-us-submit-button',
        EMAIL: '#email',
        NAME: '#name',
        NAVBAR_HIDE_MENU: '#hide-menu',
        NAVBAR_MENU: '#navbar-menu',
        NAVBAR_TOGGLE_BUTTON: '#navbar-toggle-button',
        HOME_CONTAINER: '#home-container',
        OUR_APPROACH_WRAPPER: '.our-approach-wrapper',
        PHONE: '#phone',
        REMOVE_SELECTED_BOX: '#remove-selected',
        SERVICES: '#services .services'
      },

      ERROR_MESSAGE = 'Oops something went wrong. Please try again.',
      SUCCESS_MESSAGE = 'Your message was send successfully!';

    function _alertMessage(message, type) {
      return  '<div role="alert" class="alert alert-' + type + ' alert-dismissible fade">' +
                '<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">&times;</span></button>' +
                '<p id="alert-message">' + message + '</p>'
              '</div>';
    }

    function _displayAlert(message, type) {
      var $alert;

      if (!_domEl.$alertContainer) {
        _domEl.$alertContainer = _domEl.$body.find(SEL.ALERT_CONTAINER);
      }

      $alert = $(_alertMessage(message, type));

      _domEl.$alertContainer.html('');
      _domEl.$alertContainer.append($alert);
      $alert.addClass(CLASS.IN)
    }

    function _populate_domElements() {
      _domEl.$body = $(SEL.BODY);
      _domEl.$window = $(window);
      _domEl.$homeContainer = _domEl.$body.find(SEL.HOME_CONTAINER);
      _domEl.$services = _domEl.$body.find(SEL.SERVICES);
      _domEl.$ourApproachWrapper = _domEl.$body.find(SEL.OUR_APPROACH_WRAPPER);
      _domEl.$boxes = _domEl.$ourApproachWrapper.find(SEL.BOXES);
      _domEl.$removeSelectedBox = _domEl.$ourApproachWrapper.find(SEL.REMOVE_SELECTED_BOX);
      _domEl.$contactForm = _domEl.$body.find(SEL.CONTACT_FORM);
      _domEl.$navbarMenu = _domEl.$body.find(SEL.NAVBAR_MENU);
      _domEl.$navbarHideButton = _domEl.$navbarMenu.find(SEL.NAVBAR_HIDE_MENU);
      _domEl.$navbarToggleButton = _domEl.$body.find(SEL.NAVBAR_TOGGLE_BUTTON);
    }

    function _showServicesIfNeeded() {
      var currentScrollPosition =  window.pageYOffset || document.documentElement.scrollTop;
      if (currentScrollPosition > 300) {
        _domEl.$services.addClass(CLASS.READY);
        _domEl.$window.off();
      }
    }

    /**
     * Disables/Enables the submit button based on passed param
     *
     * @method _disableSubmit
     * @param {Boolean} disable - If true, disables the submit button else enables it
     * @private
     */
    function _disableSubmitBtn(disable) {
      var setDisabled = disable || false;

      if (!_domEl.$contactUsSubmitButton) {
        _domEl.$contactUsSubmitButton = _domEl.$contactForm.find(SEL.CONTACT_US_SUBMIT_BTN);
      }

      _domEl.$contactUsSubmitButton.prop(PROP.DISABLED, setDisabled);
    }

    function _handleBoxClick(ev) {
      ev.preventDefault();
      _$selectedBoxEl = $(this);
      _domEl.$ourApproachWrapper.addClass(CLASS.CAROUSEL_MODE);
      _$selectedBoxEl.addClass(CLASS.SELECTED);

    }

    function _handleRemoveSelectedBoxClick(ev) {
      ev.preventDefault();
      _domEl.$ourApproachWrapper.removeClass(CLASS.CAROUSEL_MODE);
      _$selectedBoxEl.removeClass(CLASS.SELECTED);
      _$selectedBoxEl = null;
    }

    function _sendMessage(fields) {
      var messageBody = '\n\n\n',
        index,
        fieldsSize = fields.length,
        field;

      for (index = 0; index < fieldsSize; ++index) {
        field = fields[index];
        messageBody += '\n\n\n' + field.name + ':  ' + field.value;
      }

      messageBody += '\n\n\n';

      $.ajax({
        url: "//formspree.io/randhawap@gmail.com",
        method: "POST",
        data: {message: messageBody},
        dataType: "json",
        success: function() {
          _displayAlert(SUCCESS_MESSAGE, ALERT_TYPE.SUCCESS);
          _domEl.$contactForm[0].reset();
          _disableSubmitBtn(false);
        },
        error: function() {
          _displayAlert(ERROR_MESSAGE, ALERT_TYPE.ERROR);
          _disableSubmitBtn(false);
        }
      });
    }
    /**
     * Callback for form validation
     *
     * @method _handleContactUsFormValidation
     * @param {Object} validationData - Data containing fields against which validation was run. Also contains result of validation
     */
    function _handleContactUsFormValidation(validationData) {
      var errorField,
        errorKey,
        errorMessage;

      if (validationData.passed) {
        _disableSubmitBtn(true);
        _sendMessage(validationData.validatedFields);
        return;
      }

      if (validationData.hasOwnProperty('errorFields')) {
        errorField = validationData.errorFields[0] || nil;
        errorKey = errorField ? (errorField.name + "_" + errorField.errors[0]).toUpperCase() : 'GENERIC';
        errorMessage = ERRORS.hasOwnProperty(errorKey) ? ERRORS[errorKey] : ERRORS.GENERIC;
        _displayAlert(errorMessage, ALERT_TYPE.ERROR)
      }
    }

    function _initContactUsFormValidator() {
      var fields = [
        {
          name: 'name',
          selector: SEL.NAME,
          rules: {
            required: true,
            size: [2]
          }
        },
        {
          name: 'email',
          selector: SEL.EMAIL,
          rules: {
            required: true,
            type: $.CT.FormValidator.FIELD_TYPE.EMAIL
          }
        },
        {
          name: 'phone',
          selector: SEL.PHONE,
          rules: {}
        },
        {
          name: 'comments',
          selector: SEL.COMMENTS,
          rules: {
            required: true,
            size: [5]
          }
        }
      ];

      _contactUsFormValidator = new $.CT.FormValidator(_domEl.$contactForm, {
        validateOnSubmit: true,
        fields: fields,
        errorClass: CLASS.ERROR,
        removeErrorOn: EVENT.KEYUP,
        callback: _handleContactUsFormValidation
      });
    }

    function _handleNavbarToggleButtonClick(e) {
      e.preventDefault()
      _domEl.$navbarMenu.toggleClass(CLASS.SHOW);
    }

    function _handleNavbarHideButtonClick(e) {
      e.preventDefault()
      _domEl.$navbarMenu.removeClass(CLASS.SHOW);
    }

    /**
     * Initialize events
     *
     * @method _initEvents
     */
    function _initEvents() {
      _domEl.$window.on(EVENT.SCROLL, _showServicesIfNeeded);
      _domEl.$boxes.on(EVENT.CLICK, SEL.BOX, _handleBoxClick);
      _domEl.$removeSelectedBox.on(EVENT.CLICK, _handleRemoveSelectedBoxClick);
      _domEl.$navbarToggleButton.on(EVENT.CLICK, _handleNavbarToggleButtonClick);
      _domEl.$navbarHideButton.on(EVENT.CLICK, _handleNavbarHideButtonClick);
    }

    /**
     * Initalize the class
     *
     * @method _init
     */
    function _init() {
      _populate_domElements();

      // Show the page
      _domEl.$homeContainer.addClass(CLASS.SHOW);

      _initEvents();
      _showServicesIfNeeded();
      _initContactUsFormValidator();
    }


    //////////////////////
    // KICK START
    //////////////////////

    _init();


    //////////////////////
    // PUBLIC METHODS
    //////////////////////

  };

}());


/**
 * Initialize HomeView
 */
$(document).ready(function () {
  window.homeView = new $.CT.HomeView()
});
