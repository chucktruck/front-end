(function () {

  /**
   * Helper class for validating form fields
   * Example Usage:
   *
   *    var validator = new FormValidator($('join'), {
   *      validateOnSubmit: true,
   *      errorClass: 'error',
   *      removeErrorOn: 'focus',
   *      fields: [
   *        {
   *          name: 'firstName',
   *          selector: '#first-name',
   *          rules: {
   *            required: true
   *          }
   *        },
   *        {
   *          name: 'lastName',
   *          selector: '#lastName,
   *          rules: {
   *            required: true
   *          }
   *        },
   *        {
   *          name: 'email',
   *          selector: '#email',
   *          rules: {
   *            required: true,
   *            type: 'email'
   *          }
   *        },
   *        {
   *          name: 'password',
   *          selector: '#password',
   *          rules: {
   *            required: true,
   *            size: [6]
   *          }
   *        }
   *      ],
   *      callback: function(data) {
   *        if (data.passed) {
   *          // Submit Form
   *        } else {
   *          // Show Error Message
   *        }
   *      }
   *    });
   *
   *
   *
   * @param {JQuery} _$formEl - Form element to be validated
   * @param {Object} _config - Config for validation. Following config is accepted
   *
   *        callback            {Function}  Required  - Callback to be called after validation is done.
   *                                                    Callback is called with parameter data of type object. It consists:
   *                                                    {
   *                                                      passed: Boolean,
   *                                                      errorFields: [
   *                                                        {
   *                                                          errors: Array, //Array of errors eg. ["required", "size"]
   *                                                          name: String,  //Name of field that was passed in fields config
   *                                                          value:  String,//Value of field
   *                                                          fieldEl: JQuery//Field Element itself
   *                                                        },
   *                                                        {
   *                                                          errors: Array,
   *                                                          name: String,
   *                                                          value:  String,
   *                                                          fieldEl: JQuery
   *                                                        }
   *                                                      ]
   *                                                      validatedFields: [
   *                                                        {
   *                                                          name: String,
   *                                                          value:  String,
   *                                                          fieldEl: JQuery
   *                                                        },
   *                                                        {
   *                                                          name: String,
   *                                                          value:  String,
   *                                                          fieldEl: JQuery
   *                                                        }
   *                                                      ]
   *                                                    }
   *
   *
   *        validateOnSubmit    {Boolean}   Optional  - If true, submit event is added and validation is handeled on submit
   *                                                    @default false
   *  validateInputsOnKeyup     {Boolean}   Optional  - If true, form is validated on keyup in all the fields passed in
   *                                                    configuration and callback is called.
   *                                                    @default false
   *        errorClass          {String}    Optional  - If passed, this class is added to the field if validation fails
   *        removeErrorOn       {String}    Optional  - If passed, error class will be removed on that event in field
   *        fields              {Object}    Required  - Hash containing selector and rules for validation.
   *                                                    Key of hash should be name for field.
   *
   *                            name    {String}        Name of field. This is needed so that callback gets back name and value
   *                                                    and doesn't have to make additional DOM queries to get field's value
   *                            rules   {Object}        Validation rules for the field. Following rules are suported:
   *
   *                                    size        {Array}     Min/Max characters for the field. If check should be made against
   *                                                            max characters only pass 0 for min. eg. [0, 10]
   *                                    required    {Boolean}   If true, field is checked against empty value.
   *                                                            Using HTML5 required attribute is recommended
   *                                    test        {Function}  If you want to test the field against some function
   *                                    type        {String}    type of field. Using HTML5 type attribute is recommended.
   *                                                            Supported types for now are: email, number,
   *                                                            If type is mentioned in markup or in config. Check for 'required'
   *                                                            is enforced
   *
   * @dependency JQuery
   */
  $.CT.FormValidator = function (_$formEl, _config) {

    //////////////////////
    // PRIVATE VARS
    //////////////////////

    var _fields = [],

      _errorClass = _config.errorClass || '',

      EVENT = {
        BLUR: 'blur',
        KEYUP: 'keyup',
        SUBMIT: 'submit'
      },

      CONST = {
        TYPE: 'type',
        PASSWORD: 'password'
      };

    //////////////////////
    // PRIVATE METHODS
    //////////////////////

    /**
     * Utility method that checks if passed value is valid email
     *
     * @method testEmail
     * @param value
     * @returns {boolean}
     */
    function testEmail(value) {
      return /^([a-zA-Z0-9_\-=\.\'\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,10}|[0-9]{1,3})(\]?)$/.test(value);
    }

    /**
     * Utility method that checks if passed value is valid phone
     *
     * @method testPhone
     * @param value
     * @returns {boolean}
     */
    function testPhone(value) {
      value = value.replace(/\-|\.|\(|\)|\ /gi, '');
      return /^[+]?[0-9]+$/.test(value);
    }

    /**
     * Utility method to validate size of characters in the field
     *
     * @method testSize
     * @param {String} value
     * @param {Array} size - Array containing min and max size of the field
     * @returns {boolean}
     */
    function testSize(value, size) {
      var passed = true,
        valueLength = value.length;

      if (valueLength < size[0]) {
        //Min size not reached
        passed =  false;
      } else {
        //If min size reached, check for max size
        if (size.length > 1) {
          if (valueLength > size[1]) {
            passed = false;
          }
        }
      }

      return passed;
    }


    /**
     * Test the passed with given options
     *
     * @method _test
     * @param {Object} field - Field with some options to test
     * @returns {Object} {{value: String, errors: Array}} - Returns object containing value of field and validation
     *                                                      errors (if any)
     * @private
     */
    function _test(field) {
      var $fieldEl = field.fieldEl,
        fieldValue = (field.isPassword) ? $fieldEl.val() : $.trim($fieldEl.val()),
        valueLength = fieldValue.length,
        data = {
          value: fieldValue,
          errors: []
        };

      //if user provided their own custom test function, test the field against is
      if (field.test && $.isFunction(field.test)) {
        data.errors =  field.test($fieldEl) || [];
        return data;
      }

      //Check if field was required and it is empty
      if (field.isRequired) {
        if (valueLength === 0) {
          data.errors.push($.CT.FormValidator.RULES.REQUIRED);
          //Return errors as field was empty and we don't need to check for other errors
          return data;
        }
      }

      //Check if field is of type email and it passes validation
      if (field.isEmail) {
        if (!testEmail(fieldValue)) {
          data.errors.push($.CT.FormValidator.RULES.TYPE);
        }
      }

      if (field.isPhone && !testPhone(fieldValue)) {
        data.errors.push($.CT.FormValidator.RULES.TYPE);
      }

      if (field.isEmailPhone) {
        $fieldEl.data('isPhone', false);
        if(testPhone(fieldValue)) {
          // mark field as phone so we know we are dealing with phone and not email
          $fieldEl.data('isPhone', true);
        } else if(!testEmail(fieldValue)) {
          data.errors.push($.CT.FormValidator.RULES.TYPE);
        }
      }

      //Validate field size
      if (field.size && $.isArray(field.size)) {
        if (!testSize(fieldValue, field.size)) {
          data.errors.push($.CT.FormValidator.RULES.SIZE);
        }
      }

      return data;
    }

    /**
     * Handler for removing error event
     *
     * @method _handleErrorRemoveEvt
     * @param {Event} evt - Event Object
     * @private
     */
    function _handleErrorRemoveEvt(evt) {
      var $fieldEl = evt.data.$fieldEl;

      $fieldEl.parent().removeClass(_config.errorClass);
      //We don't need error event now. Lets unbind it
      _bindRemoveErrorEvt($fieldEl, false);
    }

    /**
     * Binds/Unbinds the event for removing error
     *
     * @method _bindRemoveErrorEvt
     * @param {JQuery} $fieldEl - Field element on which event is to be bind/unbind
     * @param {Booloean} bind - If true binds else unbinds event
     * @private
     */
    function _bindRemoveErrorEvt($fieldEl, bind) {
      if (bind) {
        $fieldEl.on(_config.removeErrorOn, {$fieldEl: $fieldEl}, _handleErrorRemoveEvt);
      } else {
        $fieldEl.unbind(_config.removeErrorOn, _handleErrorRemoveEvt);
      }
    }

    /**
     * Runs the validation on form fields that need to be validated
     *
     * @method _validate
     * @private
     */
    function _validate(ev) {
      var data,
        field,
        $fieldEl,
        errorFields = [],
        validatedFields = [],
        fieldsSize = _fields.length,
        hasErrors = false,
        idx;

      if (!_config.callback || !fieldsSize) {
        return;
      }

      for (idx = 0; idx < fieldsSize; idx++) {
        field = _fields[idx];
        $fieldEl = field.fieldEl;
        $fieldParentEl = $fieldEl.parent();

        //Test the field
        data = _test(field);

        if (_config.validateInputsOnKeyup && ev.currentTarget == $fieldEl[0] &&
            _config.errorClass && data.errors && data.errors.lenght == 0) {

          $fieldParentEl.removeClass(_config.errorClass);
        }

        if (data.errors.length) {
          hasErrors = true;
          if (_errorClass && !$fieldParentEl.hasClass(_errorClass)) {
            if (_config.validateInputsOnKeyup && ev.type != EVENT.SUBMIT) {
              if (ev.type == EVENT.KEYUP && ev.currentTarget == $fieldEl[0] && data.errors && data.errors.length > 0) {
                $fieldParentEl.addClass(_config.errorClass);
              }
            } else {
              $fieldParentEl.addClass(_config.errorClass);

              //Attach event on which error is to be removed
              if (_config.removeErrorOn) {
                _bindRemoveErrorEvt($fieldEl, true);
              }
            }
          }
          //Add error fields
          errorFields.push({
            name:     field.name,
            fieldEl:  $fieldEl,
            value:    data.value,
            errors:   data.errors
          });

        } else {
          if (_errorClass && $fieldParentEl.hasClass(_errorClass)) {
            $fieldParentEl.removeClass(_config.errorClass);

            //Unbind error removal event
            if (_config.removeErrorOn) {
              _bindRemoveErrorEvt($fieldEl, false);
            }
          }
          //Add validated fields
          validatedFields.push({
            name:     field.name,
            fieldEl:  $fieldEl,
            value:    data.value
          });
        }
      }

      _config.callback({
        passed: !hasErrors,
        errorFields: errorFields,
        validatedFields: validatedFields,
        event: ev
      });
    }

    /**
     * Handler for submit form.
     * - Prevents the default action
     * - Runs validation on the fields
     *
     * @param {Event} ev - Event object
     * @private
     */
    function _handleSubmit(ev) {
      ev.preventDefault();

      _validate(ev);
    }

    /**
     * Sanitize passed in fields and build array of fields to cache some data so that we don't query node un-necessarily
     * on each attempt to validate
     *
     * @method _initFields
     * @private
     */
    function _initFields() {
      var fields = _config.fields,
        fieldsSize,
        field,
        $fieldEl,
        rules,
        idx;


      for (idx = 0, fieldsSize = fields.length; idx < fieldsSize; idx++) {
        field = fields[idx];
        rules = field.rules;

        $fieldEl = $(field.selector);
        _fields.push({
          name:       field.name,
          fieldEl:    $fieldEl,
          isRequired: !!rules[$.CT.FormValidator.RULES.REQUIRED],
          isPassword: ($fieldEl.attr(CONST.TYPE) === CONST.PASSWORD),
          isEmail:    (rules[$.CT.FormValidator.RULES.TYPE] === $.CT.FormValidator.FIELD_TYPE.EMAIL ||
                      $fieldEl.attr(CONST.TYPE) === $.CT.FormValidator.FIELD_TYPE.EMAIL),
          isPhone:    (rules[$.CT.FormValidator.RULES.TYPE] === $.CT.FormValidator.FIELD_TYPE.PHONE ||
                      $fieldEl.attr(CONST.TYPE) === $.CT.FormValidator.FIELD_TYPE.PHONE),
          isEmailPhone: (rules[$.CT.FormValidator.RULES.TYPE] === $.CT.FormValidator.FIELD_TYPE.EMAIL_OR_PHONE ||
                        $fieldEl.attr(CONST.TYPE) === $.CT.FormValidator.FIELD_TYPE.EMAIL_OR_PHONE),
          test:       rules.test,
          size:       rules.size
        });
      }
    }

    /**
     * Initialize events
     *
     * @method _initEvents
     */
    function _initEvents() {
      if (!!_config.validateOnSubmit) {
        _$formEl.on(EVENT.SUBMIT, _handleSubmit)
      }

      if (!!_config.validateInputsOnKeyup) {
        _$formEl.delegate('input', EVENT.KEYUP, _validate)
      }
    }

    /**
     * Initalize the class
     *
     * @method _init
     */
    function _init() {
      _initFields();
      _initEvents();
    }


    //////////////////////
    // KICK START
    //////////////////////

    _init();


    //////////////////////
    // PUBLIC METHODS
    //////////////////////

    //Expose validate method so that users can call it when needed if not using validateOnSubmit option
    this.validate = _validate;
    this.handleSubmit = _handleSubmit;
  };


  //////////////////////
  // PUBLIC CONSTANTS
  //////////////////////

  /**
   * Rules that can be passed in config
   *
   * @property RULES
   */
  $.CT.FormValidator.RULES = {
    CHARACTERS: 'characters', //Character type
    INVALID: 'invalid',
    RANGE: 'range',
    REGEX: 'regex',
    REQUIRED: 'required',
    SIZE: 'size',
    TEST: 'test',
    TYPE: 'type'
  };

  /**
   * Field types that can be passed in rule 'type'. It is usually type of field
   *
   * @property FIELD_TYPE
   */
  $.CT.FormValidator.FIELD_TYPE = {
    EMAIL: 'email',
    PHONE: 'tel',
    EMAIL_OR_PHONE: 'emailOrPhone'
  };
}());
