
/**************************
 Initialize the Angular App
 **************************/

var app = angular.module("app", [
    "ngAnimate","ui.router", "ui.bootstrap.datetimepicker",
    "ngDialog","ngMessages","toaster","app.layout","app.modalModule",
    "app.home","app.login","app.order","app.company",
    "app.broker","app.company","app.driver","app.equipment",
    "app.invoice", "app.bill", "app.orderTransaction", "app.expense", "app.checkDetail", "app.distanceRate"]);

app.constant('USER_ROLES', {
    all: '*',
    ADMIN: 'ADMIN',
    DISPATCHER: 'DISPATCHER',
    ACCOUNTANT:'ACCOUNTANT'

});


// principal is a service that tracks the user's identity.
// calling identity() returns a promise while it does what you need it to do
// to look up the signed-in user's identity info. for example, it could make an
// HTTP request to a rest endpoint which returns the user's name, roles, etc.
// after validating an auth token in a cookie. it will only do this identity lookup
// once, when the application first runs. you can force re-request it by calling identity(true)
app.factory('principal', ['$q', '$http', '$timeout',
    function($q, $http, $timeout) {
        var _identity ;
        var _authenticated = false;
        return {
            isIdentityResolved: function() {
                return angular.isDefined(_identity);
            },
            isAuthenticated: function() {
                return _authenticated;
            },
            isInRole: function(role) {
                if (!_authenticated || !_identity.roles) return false;

                return _identity.roles.indexOf(role) != -1;
            },
            isInAnyRole: function(roles) {
                if (!_authenticated || !_identity.roles) return false;

                for (var i = 0; i < roles.length; i++) {
                    if (this.isInRole(roles[i])) return true;
                }

                return false;
            },
            authenticate: function(identity) {
                _identity = identity;
                _authenticated = identity !== null;

                if (identity) localStorage.setItem("user.identity", angular.toJson(identity));
                else localStorage.removeItem("user.identity");
            },
            identity: function(force) {
                var deferred = $q.defer();

                if (force === true) _identity = undefined;

                // check and see if we have retrieved the identity data from the server. if we have, reuse it by immediately resolving
                if (angular.isDefined(_identity)) {
                    deferred.resolve(_identity);

                    return deferred.promise;
                }

                var self = this;
                $timeout(function() {
                    _identity = angular.fromJson(localStorage.getItem("user.identity"));
                    self.authenticate(_identity);
                    deferred.resolve(_identity);
                }, 1000);

                return deferred.promise;
            }
        };
    }
]);
// authorization service's purpose is to wrap up authorize functionality
// it basically just checks to see if the principal is authenticated and checks the root state
// to see if there is a state that needs to be authorized. if so, it does a role check.
// this is used by the state resolver to make sure when you refresh, hard navigate, or drop onto a
// route, the app resolves your identity before it does an authorize check. after that,
// authorize is called from $stateChangeStart to make sure the principal is allowed to change to
// the desired state
app.factory('authorization', ['$rootScope', '$state', 'principal',
    function($rootScope, $state, principal) {
        return {
            authorize: function() {
                return principal.identity()
                    .then(function() {
                        var isAuthenticated = principal.isAuthenticated();

                        if ($rootScope.toState.data.roles && $rootScope.toState.data.roles.length > 0 && !principal.isInAnyRole($rootScope.toState.data.roles)) {
                            if (isAuthenticated) $state.go('home'); // user is signed in but not authorized for desired state
                            else {
                                // user is not authenticated. stow the state they wanted before you
                                // send them to the signin state, so you can return them when you're done
                                $rootScope.returnToState = $rootScope.toState;
                                $rootScope.returnToStateParams = $rootScope.toStateParams;

                                // now, send them to the signin state so they can log in
                                $state.go('home');
                            }
                        }
                    });
            }
        };
    }
]);



app.run(
    function($rootScope,$location, $state, $stateParams, authorization, principal) {

        $(document).ready(function(config){

            setTimeout(function(){
                $('.page-loading-overlay').addClass("loaded");
                $('.load_circle_wrapper').addClass("loaded");
            },1000);

        });
        $rootScope.$on('$stateChangeStart', function(event, toState, toStateParams) {
            $rootScope.toState = toState;
            $rootScope.toStateParams = toStateParams;

            if (principal.isIdentityResolved()) authorization.authorize();
        });
    }

 );

app.config(function ($httpProvider,$stateProvider, $urlRouterProvider,$locationProvider) {

    $httpProvider.defaults.withCredentials = true;
    $stateProvider
        .state('app', {
            url: '',
            abstract: true,
            views: {
                'navigation.header': {
                    templateUrl: "app/views/layout/top-header.html",
                    controller:'LayoutCtrl'
                	},
                 'navigation.footer': {
                    templateUrl: "app/views/layout/footer.html",
                    controller:'LayoutCtrl'
                     },
                'navigation.sidenav': {
                    templateUrl: "app/views/layout/side-nav.html",
                    controller:'LayoutCtrl'                    
                }
            }

        })
    ;
    $urlRouterProvider.otherwise('/home');
    $locationProvider.html5Mode(false);

});
