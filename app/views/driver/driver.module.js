angular.module("app.driver",[])
    .config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('app.driver', {
            url: '/driver',
            abstract: true
        })

        .state('app.driver.all', {
            url: '/list',
            params:{
                mode:'list'
            },
            views : {
                "@": {
                    templateUrl: "app/views/driver/list.html",
                    controller:'DriverCtrl'
                }
            }
        });
});
