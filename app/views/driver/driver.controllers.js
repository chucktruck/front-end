
angular.module("app.driver").controller("DriverCtrl",
    function ($scope,$http,$location,$filter,ngDialog,toaster, modalService, statesService) {

        $scope.drivers=[];
        $scope.driver={};

        $scope.selected= {};
        // gets the template to ng-include for a table row / item
        $scope.getTemplate = function (driver) {
            if (driver.id === $scope.selected.id) return 'edit';
            else return 'display';
        };

        $scope.edit = function (driver) {
            $scope.selected = angular.copy(driver);
        };
        
        $scope.delete = function (driver,idx) {
            if (confirm("Do you want to delete ? Please confirm.")) {

                console.log("Deleting driver");
                return $http.post('/api/driver/delete', driver ).then(
                    function handleSuccess(res) {
                        $scope.drivers.splice(idx, 1);
                        $scope.search();
                        $scope.reset();

                    },
                    function handleError(error) {
                        toaster.pop('error',"","something wrong happened !! Please report to Admin");
                        $scope.reset();
                    });
            }
        };

        $scope.update = function (idx) {
            console.log("Saving driver");
            return $http.post('/api/driver/update', $scope.selected ).then(
                function handleSuccess(res) {
                    $scope.drivers[idx] = angular.copy($scope.selected);
                    $scope.search();
                    $scope.reset();

                },
                function handleError(error) {
                    toaster.pop('error',"","something wrong happened !! Please report to Admin");
                    $scope.reset();
                });


        };

        $scope.reset = function () {
            $scope.selected = {};
        };


        $scope.ok = function () {
            $scope.driver.isOwnerOperator = $scope.driver.isOwnerOperator == 'Yes' ? true : false;
            return $http.post('/api/driver/save', $scope.driver).then(
                function handleSuccess(res) {
                    $scope.drivers.push(res.data);
                    $scope.driver = {};
                    $scope.search();
                    toaster.pop('success',"","New Driver Created");
                },
                function handleError(error) {
                    toaster.pop('error',"","something wronng happened !!");
                });
        };
        $scope.addDriver = function() {
            ngDialog.open({
                template: '/app/views/driver/add.html',
                className: 'ngdialog-theme-default custom-width-600',
                width:600,
                scope: $scope,
                closeByDocument: false,
                closeByEscape: false
            });
        };
        $scope.getStates = function() {
            return statesService.getUSStates();
        };

        $http.get('/api/driver/list', $scope).then(
            function handleSuccess(res) {
                $scope.drivers=res.data;
                return $scope.drivers, $scope.searchKeywords = "", $scope.filteredList = [], $scope.row = "", $scope.select = function (page) {
                    var end, start;
                    return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageList = $scope.filteredList.slice(start, end);
                }, $scope.onFilterChange = function () {
                    return $scope.select(1), $scope.currentPage = 1, $scope.row = "";
                }, $scope.onNumPerPageChange = function () {
                    return $scope.select(1), $scope.currentPage = 1;
                }, $scope.onOrderChange = function () {
                    return $scope.select(1), $scope.currentPage = 1;
                }, $scope.search = function () {
                    return $scope.filteredList = $filter("filter")($scope.drivers, $scope.searchKeywords), $scope.onFilterChange();
                }, $scope.order = function (rowName) {
                    return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredList = $filter("orderBy")($scope.drivers, rowName), $scope.onOrderChange()) : void 0;
                }, $scope.numPerPageOpt = [3, 5, 10, 20], $scope.numPerPage = $scope.numPerPageOpt[2], $scope.currentPage = 1, $scope.currentPageList = [], (init = function () {
                    return $scope.search(), $scope.select($scope.currentPage);
                }), $scope.search();
            },
            function handleError(error) {
                return function () {
                };
            });
    }
);