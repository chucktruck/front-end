
angular.module("app.checkDetail").controller("CheckDetailCtrl",
    function ($scope,$http,$location,$filter,ngDialog,toaster) {
        var init;
        $scope.checkDetails=[];
        $scope.checkDetail={};
        $scope.selected= {};
        $scope.checkDetailArray = [];
        // gets the template to ng-include for a table row / item
        $scope.getTemplate = function (checkDetail) {
            if (checkDetail.id === $scope.selected.id) return 'edit';
            else return 'display';
        };

        $scope.edit = function (checkDetail) {
            $scope.selected = angular.copy(checkDetail);
        };

        $scope.addCheckDetail = function() {
            ngDialog.open({
                template: '/app/views/checkDetail/add.html',
                className: 'ngdialog-theme-default custom-width-400',
                width:400,
                scope: $scope,
                closeByDocument: false,
                closeByEscape: false
            });
        };
        $scope.delete = function (checkDetail,idx) {
            if (confirm("Do you want to delete ? Please confirm.")) {

            return $http.post('/api/check/'+idx+'/delete').then(
                function handleSuccess(res) {
                    $scope.checkDetails.splice(idx, 1);
                    $scope.search();
                },
                function handleError(error) {
                    toaster.pop('error',"","something wrong happened !! Please report to Admin");
                });
            }
        };

        $scope.update = function (idx) {
            console.log("Saving Check Detail");
            return $http.post('/api/order/'+$scope.$parent.selectedOrder.orderId+'/expense/update', $scope.selected ).then(
                function handleSuccess(res) {
                    $scope.checkDetails[idx] = angular.copy($scope.selected);
                    $scope.search();
                    $scope.reset();

                },
                function handleError(error) {
                    toaster.pop('error',"","something wrong happened !! Please report to Admin");
                    $scope.reset();
                });
        };

        $scope.reset = function () {
            $scope.selected = {};
        };

        $scope.ok = function() {
            
            var id;
            var type;
            var URL = `/api`;
            var isCheckAmountVerified = false;
            if($scope.$parent.selectedInvoice) {
                id = $scope.$parent.selectedInvoice.id;
                type = "INVOICE"; 
                var URL = `${URL}/invoice/`;
            } else if($scope.$parent.selectedBill) {
                id = $scope.$parent.selectedBill.id;
                type = "BILL";
                var URL = `${URL}/bill/`;
            }          
            URL = `${URL}/${id}/check/save`;  
            var invoice = $scope.$parent.selectedInvoice ? $scope.$parent.selectedInvoice 
                                                        : $scope.$parent.selectedBill;
            if($scope.checkDetail.amount > Math.abs(invoice.balance)) {
                var warning = `Check Amount: ${$scope.checkDetail.amount} more than current balance ${Math.abs(invoice.balance)}.
                Do you want to proceed ?`;
                if(confirm(warning) === true) {
                    isCheckAmountVerified = true;
                } else {
                    isCheckAmountVerified = false;
                }
            } else {
                isCheckAmountVerified = true;
            }

            if(isCheckAmountVerified) {
                return $http.post(URL, $scope.checkDetail).then(
                function handleSuccess(res) {
                    $scope.checkDetails.push(res.data);
                    $scope.search();
                    toaster.pop('success',"","Check information saved. Invoice/Bill updated.");
                },
                function handleError(error) {
                    toaster.pop('error',"","something wrong happened !! Please report to Admin");
                }); 
            } else {
                toaster.pop('Fail',"","Check information not submitted.");
            }
        };


        var URL = '/api/check/list';
        var id;
        var type;
        if($scope.$parent.selectedInvoice) {
            id = $scope.$parent.selectedInvoice.id;
            type = "INVOICE"; 
        } else if($scope.$parent.selectedBill) {
            id = $scope.$parent.selectedBill.id;
            type = "BILL";
        }
        $http.get(URL, {
            params: {
                accountId: id,
                accountType: type
            }
            }).then(
            function handleSuccess(res) {
                $scope.checkDetails=res.data;
                // return $scope.checkDetails, $scope.searchKeywords = "", $scope.filteredCheckDetails = [], $scope.row = "", $scope.select = function (page) {
                //     var end, start;
                //     return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageChecks = $scope.filteredCheckDetails.slice(start, end);
                // }, $scope.onFilterChange = function () {
                //     return $scope.select(1), $scope.currentPage = 1, $scope.row = "";
                // }, $scope.onNumPerPageChange = function () {
                //     return $scope.select(1), $scope.currentPage = 1;
                // }, $scope.onOrderChange = function () {
                //     return $scope.select(1), $scope.currentPage = 1;
                // }, $scope.search = function () {
                //     return $scope.filteredCheckDetails = $filter("filter")($scope.checkDetail, $scope.searchKeywords), $scope.onFilterChange();
                // }, $scope.order = function (rowName) {
                //     return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredCheckDetails = $filter("orderBy")($scope.checkDetail, rowName), $scope.onOrderChange()) : void 0;
                // }, $scope.numPerPageOpt = [3, 5, 10, 20], $scope.numPerPage = $scope.numPerPageOpt[2], $scope.currentPage = 1, $scope.currentPageChecks = [], (init = function () {
                //     return $scope.search(), $scope.select($scope.currentPage);
                // }), $scope.search();
            },
            function handleError(error) {
                return function () {
                };
            });
    }
);




