angular.module("app.checkDetail",[])
    .config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('app.checkDetail', {
            url: '/checkDetails',
            abstract: true

        })

        .state('app.checkDetail.all', {
            url: '/list',
            params:{
                mode:'list'
            },
            views : {
                "@": {
                    templateUrl: "app/views/checkDetail/list.html",
                    controller:'CheckDetailCtrl'
                }
            }

        });
});
