angular.module('app.company', [])
    .config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('app.company', {
                url: '/company',
                abstract: true
            })

            .state('app.company.all', {
                url: '/list',
                params:{
                    mode:'list'
                },
                views : {
                    "@": {
                        templateUrl: "app/views/company/list.html",
                        controller:'CompanyCtrl'

                    }
                }
            });
    });
