
angular.module("app.company").controller("CompanyCtrl",
    function ($scope,$http,$location,$filter,ngDialog,$timeout, $q,toaster, statesService) {

        $scope.companies=[];
        $scope.company={};
        $scope.distance = {};

        $scope.selected= {};
        // gets the template to ng-include for a table row / item
        $scope.getTemplate = function (company) {
            if (company.id === $scope.selected.id) return 'edit';
            else return 'display';
        };

        $scope.edit = function (company) {
            $scope.selected = angular.copy(company);
        };

        $scope.getStates = function() {
            return statesService.getUSStates();
        };

        $scope.delete = function (company,idx) {
            if (confirm("Do you want to delete ? Please confirm.")) {

                console.log("Deleting company");
                return $http.post('/api/company/delete', company ).then(
                    function handleSuccess(res) {
                        $scope.companies.splice(idx, 1);
                        $scope.search();
                        $scope.reset();

                    },
                    function handleError(error) {
                        toaster.pop('error',"","something wrong happened !! Please report to Admin");
                        $scope.reset();
                    });
            }
        };

        $scope.update = function (idx) {
            console.log("Saving company");
            return $http.post('/api/company/update', $scope.selected ).then(
                function handleSuccess(res) {
                    $scope.companies[idx] = angular.copy($scope.selected);
                    $scope.search();
                    $scope.reset();

                },
                function handleError(error) {
                    toaster.pop('error',"","something wrong happened !! Please report to Admin");
                    $scope.reset();
                });


        };

        $scope.reset = function () {
            $scope.selected = {};
        };


        var self = $scope;

// list of `state` value/display objects
        self.selectedStateItem  = null;
        self.searchText    = null;
        self.querySearch   = querySearch;

        $scope.ok = function () {
            return $http.post('/api/company/save', $scope.company).then(
                function handleSuccess(res) {
                    $scope.companies.push(res.data);
                    $scope.company = {};
                    $scope.search();
                    toaster.pop('success',"","New Company Created");
                    if($scope.companyArray) {
                        $scope.companyArray.push(res.data);
                    }
                },
                function handleError(error) {
                    toaster.pop('error',"","something wronng happened !!");
                });
        };
        $scope.addCompany = function() {
            ngDialog.open({
                template: '/app/views/company/add.html',
                className: 'ngdialog-theme-default custom-width-600',
                width:600,
                scope: $scope,
                closeByDocument: false,
                closeByEscape: false
            });
        };
        $http.get('/api/company/list', $scope).then(
            function handleSuccess(res) {
                $scope.companies=res.data;
                return $scope.companies, $scope.searchKeywords = "", $scope.filteredList = [], $scope.row = "", $scope.select = function (page) {
                    var end, start;
                    return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageList = $scope.filteredList.slice(start, end);
                }, $scope.onFilterChange = function () {
                    return $scope.select(1), $scope.currentPage = 1, $scope.row = "";
                }, $scope.onNumPerPageChange = function () {
                    return $scope.select(1), $scope.currentPage = 1;
                }, $scope.onOrderChange = function () {
                    return $scope.select(1), $scope.currentPage = 1;
                }, $scope.search = function () {
                    return $scope.filteredList = $filter("filter")($scope.companies, $scope.searchKeywords), $scope.onFilterChange();
                }, $scope.order = function (rowName) {
                    return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredList = $filter("orderBy")($scope.companies, rowName), $scope.onOrderChange()) : void 0;
                }, $scope.numPerPageOpt = [3, 5, 10, 20], $scope.numPerPage = $scope.numPerPageOpt[2], $scope.currentPage = 1, $scope.currentPageList = [], (init = function () {
                    return $scope.search(), $scope.select($scope.currentPage);
                }), $scope.search();
            },
            function handleError(error) {
                return function () {
                };
            });



        // ******************************
        // Internal methods
        // ******************************

        /**
         * Search for states... use $timeout to simulate
         * remote dataservice call.
         */
        function querySearch (query) {
            var results = query ? self.states.filter( createFilterFor(query) ) : self.states;
            var deferred = $q.defer();
            $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
            return results;
        }


        /**
         * Create filter function for a query string
         */
        function createFilterFor(query) {
            var lowercaseQuery = angular.lowercase(query);

            return function filterFn(state) {
                return (state.value.indexOf(lowercaseQuery) === 0);
            };

        }


    }
);



