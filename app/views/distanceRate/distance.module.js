angular.module('app.distanceRate', [])
    .config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('app.distanceRate', {
                url: '/distanceRate',
                abstract: true
            })

            .state('app.distanceRate.all', {
                url: '/list',
                params:{
                    mode:'list'
                },
                views : {
                    "@": {
                        templateUrl: "app/views/distanceRate/rate.html",
                        controller:'DistanceRateCtrl'

                    }
                }
            });
    });