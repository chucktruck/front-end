
angular.module("app.distanceRate").controller("DistanceRateCtrl",
    function ($scope,$http,$location,$filter,ngDialog,$timeout, $q,$state,toaster, statesService) {
        var init;
        $scope.distanceRates=[];
        $scope.distanceRate={};
        $scope.selected= {};
        // gets the template to ng-include for a table row / item
        $scope.getTemplate = function (distanceRate) {
            if (distanceRate.id === $scope.selected.id) return 'edit';
            else return 'display';
        };

        $scope.edit = function (distanceRate) {
            $scope.selected = angular.copy(distanceRate);
        };

        $scope.delete = function (distanceRate,idx) {
            if (confirm("Do you want to delete ? Please confirm.")) {

            console.log("Deleting Rate:");
            return $http.post('/api/distancerate/delete', distanceRate ).then(
                function handleSuccess(res) {
                    $scope.distanceRates.splice(idx, 1);
                    $scope.search();
                    $scope.reset();

                },
                function handleError(error) {
                    toaster.pop('error',"","something wrong happened !! Please report to Admin");
                    $scope.reset();
                });
            }
        };

        $scope.update = function (idx) {
            console.log("Saving custom Rate");
            return $http.post('/api/distancerate/update', $scope.selected ).then(
                function handleSuccess(res) {
                    $scope.distanceRates[idx] = angular.copy($scope.selected);
                    $scope.search();
                    $scope.reset();

                },
                function handleError(error) {
                    toaster.pop('error',"","something wrong happened !! Please report to Admin");
                    $scope.reset();
                });


        };

        $scope.reset = function () {
            $scope.selected = {};
        };

        $scope.ok = function () {
             return $http.post('/api/distancerate/save', $scope.distanceRate).then(
                function handleSuccess(res) {
                    $scope.distanceRates.push(res.data);
                    $scope.distanceRate = {};
                    $scope.search();
                    toaster.pop('success',"","New Custom Distance Rate Created");
                    // $scope.expenseArray.push(res.data);
                    // $scope.dispatchOrder.selectedexpenseItem  = res.data;
                    // $scope.dispatchOrder.searchexpenseText    = $scope.expense.name;
                },
                function handleError(error) {
                    toaster.pop('error',"","something wrong happened !! Please report to Admin");
                });
        };
        $scope.getStates = function() {
            return statesService.getUSStates();
        };        

        $scope.addDistanceRate = function() {
            ngDialog.open({
                template: '/app/views/distanceRate/add.html',
                className: 'ngdialog-theme-default custom-width-600',
                scope: $scope,
                closeByDocument: false,
                closeByEscape: false
            });
        };
        $http.get('/api/distancerate/list', $scope).then(
            function handleSuccess(res) {
                $scope.distanceRates=res.data;
                return $scope.distanceRates, $scope.searchKeywords = "", $scope.filteredList = [], $scope.row = "", $scope.select = function (page) {
                    var end, start;
                    return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageDistanceRates = $scope.filteredList.slice(start, end);
                }, $scope.onFilterChange = function () {
                    return $scope.select(1), $scope.currentPage = 1, $scope.row = "";
                }, $scope.onNumPerPageChange = function () {
                    return $scope.select(1), $scope.currentPage = 1;
                }, $scope.onOrderChange = function () {
                    return $scope.select(1), $scope.currentPage = 1;
                }, $scope.search = function () {
                    return $scope.filteredList = $filter("filter")($scope.distanceRates, $scope.searchKeywords), $scope.onFilterChange();
                }, $scope.order = function (rowName) {
                    return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredList = $filter("orderBy")($scope.distanceRates, rowName), $scope.onOrderChange()) : void 0;
                }, $scope.numPerPageOpt = [3, 5, 10, 20], $scope.numPerPage = $scope.numPerPageOpt[2], $scope.currentPage = 1, $scope.currentPageDistanceRates = [], (init = function () {
                    return $scope.search(), $scope.select($scope.currentPage);
                }), $scope.search();
            },
            function handleError(error) {
                return function () {
                };
            });


        $scope.companyArray = loadAllCompany();
        self.selectedOriginItem  = null;
        self.searchOriginText    = null;
        self.selectedDestinationItem  = null;
        self.searchDestinationText    = null;
        self.querySearchCompany  = querySearchCompany;

        function loadAllCompany() {
            $http.get('/api/company/list', $scope).then(
                function handleSuccess(res) {
                    $scope.companyArray = res.data;
                },
                function handleError(error) {
                    return function () {
                    };
                });
        }

       function querySearchCompany (query) {
            var results = query ? self.companyArray.filter( createFilterFor(query) ) : self.companyArray;
            var deferred = $q.defer();
            $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
            return results;
        }

        /**
         * Create filter function for a query string
         */
        function createFilterFor(query) {
            var lowercaseQuery = angular.lowercase(query);

            return function filterFn(filterObject) {
                return (angular.lowercase(filterObject.name).indexOf(lowercaseQuery) === 0);
            };

        }
        
    }
);



