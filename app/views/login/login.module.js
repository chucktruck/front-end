angular.module('app.login', ['ui.router'])
    .config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('signin', {
                url: '/signin',
                templateUrl: "app/views/login/signin.html",
                controller: 'signinCtrl',
                data: {
                    roles: []
                }

            });

        $stateProvider
            .state('signup', {
                url: '/signup',
                templateUrl: "app/views/login/registration.html",
                controller: 'signupCtrl',
                data: {
                    roles: []
                }

            });

        $stateProvider
            .state('register', {
                url: '/signup/:planType',
                templateUrl: "app/views/login/registration.html",
                controller: 'signupCtrl',
                data: {
                    roles: []
                }

            });            

        $stateProvider
            .state('app.login', {
                url: '/login',
                abstract: true
            })
            .state('app.login.all', {
                modal: true,
                url: '/list',
                templateUrl: "app/views/login/list.html"
            });
        $stateProvider
            .state('app.login.adduser', {
                url: '/save',
                templateUrl: "app/views/login/newuser.html",
                controller: 'userCtrl',
                data: {
                    roles: []
                }

            });                
    });
