angular.module('app.login')
    .controller("signinCtrl", function ($scope,$http,$location,$rootScope,$state, $filter, principal,ngDialog, toaster, modalService) {
        var config = {
            ignoreAuthModule: 'ignoreAuthModule',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        };
        $scope.user={};
        $scope.users =[];
        $scope.newUser={};

        $scope.selected= {};
        $scope.ok = function () {
            return $http.post('/api/user/save', $scope.newUser).then(

            function handleSuccess(res) {
                $scope.users.push(res.data);
                $scope.search();
                
                toaster.pop('success',"","New User Created");
            },
            function handleError(error) {
                toaster.pop('error',"","New User Create Failed.");
            });
        };

        $scope.addUser = function() {
            ngDialog.open({
                template: '/app/views/login/newuser.html',
                className: 'ngdialog-theme-default custom-width-600',
                width:600,
                scope: $scope,
                closeByDocument: false,
                closeByEscape: false
            });
        };

        $scope.listUsers = function() {
            var modalDefaults = {
                backdrop: true,
                keyboard: true,
                modalFade: true,
                templateUrl: '/app/partials/userModal.html'
            };
            var modalOptions = {
                    closeButtonText: 'Cancel',
                    actionButtonText: 'Users',
                    headerText: 'Users',
                    bodyText: '',
                };
                $http.get('/api/user/list').then(
                    function handleSuccess(res) {
                        $scope.users = res.data;
                        modalOptions.users = $scope.users;
                        modalService.showModal(modalDefaults, modalOptions).then(function (result) {
                        });
                    });        
        };

        // gets the template to ng-include for a table row / item
        $scope.getTemplate = function (user) {
            if (user.id === $scope.selected.id) return 'edit';
            else return 'display';
        };

        $scope.editUser = function (user) {
            $scope.selected = angular.copy(user);
        };

        $scope.deleteUser = function (user,idx) {
            if (confirm("Do you want to delete ? Please confirm.")) {

            console.log("Deleting user");
            return $http.post('/api/user/delete', user ).then(
                function handleSuccess(res) {
                    $scope.users.splice(idx, 1);
                    $scope.search();
                    $scope.reset();

                },
                function handleError(error) {
                    toaster.pop('error',"","something wrong happened !! Please report to Admin");
                    $scope.reset();
                });
            }
        };

        $scope.updateUser = function (idx) {
            console.log("Saving User");
            return $http.post('/api/user/update', $scope.selected ).then(
                function handleSuccess(res) {
                    $scope.users[idx] = angular.copy($scope.selected);
                    $scope.search();
                    $scope.reset();

                },
                function handleError(error) {
                    toaster.pop('error',"","something wrong happened !! Please report to Admin");
                    $scope.reset();
                });


        };

        $scope.reset = function () {
            $scope.selected = {};
        };    

        $scope.userRoles = function() {
            var data = ['ADMIN', 'DISPATCHER', 'ACCOUNTANT'];
            return data;
        };

        $http.get('/api/user/list', $scope).then(
            function handleSuccess(res) {
                $scope.users=res.data;
                return $scope.users;
            },
            function handleError(error) {
                return function () {
            };
        });

        $scope.logout = function() {
            $http.post('/logout',{}).success(function() {
                $rootScope.authenticated = false;
                $location.path("/");
            }).error(function(error) {
                $rootScope.authenticated = false;
            });
        };

        var authenticate = function(credentials, callback) {

                var headers = credentials ? {authorization : "Basic "
                    + btoa(credentials.name + ":" + credentials.password)
                } : {};

                $http.get('/user', {headers : headers}).success(function(data) {
                if (data.name) {
                    console.log('Console');
                    $rootScope.authenticated = true;
                } else {
                    $rootScope.authenticated = false;
                }
                callback && callback();
                }).error(function(error) {
                    console.log(error);
                $rootScope.authenticated = false;
                callback && callback();
                });

            };

            authenticate();
            // var formElement = document.getElementById('join-form');
            // formElement.addEventListener('login', function(e){
            //     alert('Got Event');
            // }, false);
            $scope.login = function() {
                $http.post('/api/login', $.param({
                    username: $scope.user.name,
                    password: $scope.user.password
                    }), config)
                    .then(
                    function handleSuccess(res) {
                        $rootScope.user=res.data;
                        principal.authenticate({
                            name: $rootScope.user.username,
                            roles: [$rootScope.user.role]
                        });
                        if ($scope.returnToState) {
                            $state.go($scope.returnToState.name, $scope.returnToStateParams);
                        }
                        else {
                            if($rootScope.user.role == "ADMIN" ||  $rootScope.user.role == "DISPATCHER"){
                                $state.go('app.order.all');
                            } else if($rootScope.user.role == "ACCOUNTANT" ){
                                $state.go('app.invoice.all');
                            }
                        }
                    },
                    function handleError(error) {
                        $scope.errorMessage="Invalid User or Password";
                            return function () {
                                return { success: false, message: "Invalid User or Password" };
                        };
                    });
                };
    })
    .controller("signupCtrl", function ($scope,$http,$location,$rootScope,$filter,ngDialog,toaster,$state) {
    var original;
    $scope.planType = "None";
    (function(){

        console.log("Registration Page Loaded " + location.href);
        var selectedPlan = location.href.split('/')[5];
        if(selectedPlan) {
            if( selectedPlan.toUpperCase() === "PREMIUM" || 
                selectedPlan.toUpperCase() === "REGULAR") {
                    $scope.planType = selectedPlan;
            } 
        }
    })();

    $scope.selectPlan = function(selectedPlan) {
        $scope.planType = selectedPlan;
        window.location.hash = "#/signup/"+selectedPlan;
        this.closeThisDialog();
    }
    $scope.changePlan = function () {
        ngDialog.open({
            template: '/app/partials/plans.html',
            className: 'ngdialog-theme-default custom-width-800',
            controller: 'signupCtrl',
            scope: $scope,
            closeByDocument: false,
            closeByEscape: true
        });        
    }
    //$scope.tenant;
    return $scope.user = {
        name: "",
        email: "",
        password: "",
        confirmPassword: "",
        age: ""
    }, $scope.showInfoOnSubmit = !1, original = angular.copy($scope.user), $scope.revert = function () {
        return $scope.user = angular.copy(original), $scope.form_signup.$setPristine(), $scope.form_signup.confirmPassword.$setPristine();
    }, $scope.canRevert = function () {
        return !angular.equals($scope.user, original) || !$scope.form_signup.$pristine;
    }, $scope.canSubmit = function () {
        return $scope.form_signup.$valid && !angular.equals($scope.user, original);
    }, $scope.submitForm = function () {
        return $http.post('/api/tenant/registration', $scope.user).then(
            function handleSuccess(res) {
                toaster.pop('success',"","Register Successfully ! Please Login ");
                $state.go('signin');
            },
            function handleError(error) {
                toaster.pop('error',"","something wrong happened !!");
            });

        //return $scope.showInfoOnSubmit = !0, $scope.revert();
    };
})
.controller("userCtrl", function($scope, $http, $filter, ngDialog, $toaster, $state) {
    $scope.user={};
    $scope.users =[];

    $scope.ok = function () {
        return $http.post('/api/user/save', $scope.user).then(

        function handleSuccess(res) {
            $scope.users.push(res.data);
            toaster.pop('success',"","New User Created");
        },
        function handleError(error) {
            toaster.pop('error',"","New Driver Create Failed.");
        });
    };
})
.directive("validateEquals",
    function () {
        return {
            require: "ngModel",
            link: function (scope, ele, attrs, ngModelCtrl) {
                var validateEqual;
                return validateEqual = function (value) {
                    var valid;
                    return valid = value === scope.$eval(attrs.validateEquals), ngModelCtrl.$setValidity("equal", valid), "function" == typeof valid ? valid({
                        value: void 0
                    }) : void 0;
                }, ngModelCtrl.$parsers.push(validateEqual), ngModelCtrl.$formatters.push(validateEqual), scope.$watch(attrs.validateEquals, function (newValue, oldValue) {
                    return newValue !== oldValue ? ngModelCtrl.$setViewValue(ngModelCtrl.$ViewValue) : void 0;
                });
            }
        };
    }
);
