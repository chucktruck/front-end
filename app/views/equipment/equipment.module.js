angular.module("app.equipment",[])
    .config(function ($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('app.equipment', {
                url: '/equipment',
                abstract: true
            })

            .state('app.equipment.all', {
                url: '/list',
                params:{
                    mode:'list'
                },
                views : {
                    "@": {
                        templateUrl: "app/views/equipment/list.html",
                        controller:'EquipmentCtrl'
                    }
                }
            });
    });