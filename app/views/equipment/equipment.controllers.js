
angular.module("app.equipment").controller("EquipmentCtrl",
    function ($scope,$http,$location,$filter,ngDialog,toaster, statesService) {

        $scope.street="";
        $scope.city="";
        $scope.state="";
        $scope.zip="";
        $scope.equipments=[];
        $scope.equipment={};
        $scope.selected= {};
        // gets the template to ng-include for a table row / item
        $scope.getTemplate = function (equipment) {
            if (equipment.id === $scope.selected.id) return 'edit';
            else return 'display';
        };

        $scope.edit = function (equipment) {
            $scope.selected = angular.copy(equipment);
        };

        $scope.getStates = function() {
            return statesService.getUSStates();
        };

        $scope.delete = function (equipment,idx) {
            if (confirm("Do you want to delete ? Please confirm.")) {

                console.log("Deleting equipment");
                return $http.post('/api/equipment/delete', equipment ).then(
                    function handleSuccess(res) {
                        $scope.equipments.splice(idx, 1);
                        $scope.search();
                        $scope.reset();

                    },
                    function handleError(error) {
                        toaster.pop('error',"","something wrong happened !! Please report to Admin");
                        $scope.reset();
                    });
            }
        };

        $scope.update = function (idx) {
            console.log("Saving equipment");
            return $http.post('/api/equipment/update', $scope.selected ).then(
                function handleSuccess(res) {
                    $scope.equipments[idx] = angular.copy($scope.selected);
                    $scope.search();
                    $scope.reset();

                },
                function handleError(error) {
                    toaster.pop('error',"","something wrong happened !! Please report to Admin");
                    $scope.reset();
                });
        };

        $scope.reset = function () {
            $scope.selected = {};
        };

        $scope.ok = function () {
            return $http.post('/api/equipment/save', $scope.equipment).then(
                function handleSuccess(res) {
                    $scope.equipments.push(res.data);
                    $scope.equipment = {};
                    $scope.search();
                    toaster.pop('success',"","New Equipment Created");
                    if($scope.equipmentArray) {
                        $scope.equipmentArray.push(res.data);
                    }
                },
                function handleError(error) {
                    toaster.pop('error',"","something wronng happened !!");
                });
        };
        $scope.addEquipment = function() {
            ngDialog.open({
                template: '/app/views/equipment/add.html',
                className: 'ngdialog-theme-default custom-width-600',
                width:600,
                scope: $scope,
                closeByDocument: false,
                closeByEscape: false
            });
        };
        $http.get('/api/equipment/list', $scope).then(
            function handleSuccess(res) {
                $scope.equipments=res.data;
                return $scope.equipments, $scope.searchKeywords = "", $scope.filteredList = [], $scope.row = "", $scope.select = function (page) {
                    var end, start;
                    return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageList = $scope.filteredList.slice(start, end);
                }, $scope.onFilterChange = function () {
                    return $scope.select(1), $scope.currentPage = 1, $scope.row = "";
                }, $scope.onNumPerPageChange = function () {
                    return $scope.select(1), $scope.currentPage = 1;
                }, $scope.onOrderChange = function () {
                    return $scope.select(1), $scope.currentPage = 1;
                }, $scope.search = function () {
                    return $scope.filteredList = $filter("filter")($scope.equipments, $scope.searchKeywords), $scope.onFilterChange();
                }, $scope.order = function (rowName) {
                    return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredList = $filter("orderBy")($scope.equipments, rowName), $scope.onOrderChange()) : void 0;
                }, $scope.numPerPageOpt = [3, 5, 10, 20], $scope.numPerPage = $scope.numPerPageOpt[2], $scope.currentPage = 1, $scope.currentPageList = [], (init = function () {
                    return $scope.search(), $scope.select($scope.currentPage);
                }), $scope.search();
            },
            function handleError(error) {
                return function () {
                };
            });
    }
);




