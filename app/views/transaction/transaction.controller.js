angular.module("app.orderTransaction").controller("OrderTransactionCtrl",
    function ($scope,$http,$location,$filter,ngDialog,$timeout, $q,$state,toaster, modalService) {
        var init;

 		var self = $scope;


        self.companyArray = loadAllCompany();
        self.selectedDestinationItem  = null;
        self.searchDestinationText    = null;
        self.selectedOriginItem  = null;
        self.searchOriginText    = null;
        self.querySearchCompany  = querySearchCompany;



        $scope.orderTransaction = {};
        $scope.transactionTypes = [ 'DELIVERY', 'YARD_IN', 'YARD_PULL', 'TERMINATION' ];
        $scope.editTransaction = function() {
        	console.log();
        };
 		
 		function loadAllCompany() {
            $http.get('/api/company/list', $scope).then(
                function handleSuccess(res) {
                	console.log("Load all Company "+res);
                    self.companyArray=res.data;
                },
                function handleError(error) {
                    return function () {
                    };
                });
        }


        function querySearchCompany (query) {
            var results = query ? self.companyArray.filter( createFilterFor(query) ) : self.companyArray;
            var deferred = $q.defer();
            console.log("query Search "+query);
            $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
            return results;
        }

/**
         * Create filter function for a query string
         */
        function createFilterFor(query) {
            var lowercaseQuery = angular.lowercase(query);

            return function filterFn(filterObject) {
                return (angular.lowercase(filterObject.name).indexOf(lowercaseQuery) === 0);
            };

        }

        $scope.saveLeg = function () {
            $scope.orderTransaction.originId = this.selectedOriginItem.id;
            $scope.orderTransaction.destinationId = this.selectedDestinationItem.id;
            $scope.orderTransaction.orderId = $scope.selectedOrder.orderId;
            $scope.orderTransaction.equipment = $scope.selectedOrder.equipment;
            $scope.orderTransaction.status = 'NEW';
            return $http.post('/api/orderLeg/save', $scope.orderTransaction).then(
            function handleSuccess(res) {
                    $state.go('app.order.all');
                $http.get('/api/order/list', $scope).then(
                    function handleSuccess(res) {
                        $scope.orders=res.data;
                        return $scope.orders, $scope.searchKeywords = "", $scope.filteredList = [], $scope.row = "", $scope.select = function (page) {
                            var end, start;
                            return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageList = $scope.filteredList.slice(start, end);
                        }, $scope.onFilterChange = function () {
                            return $scope.select(1), $scope.currentPage = 1, $scope.row = "";
                        }, $scope.onNumPerPageChange = function () {
                            return $scope.select(1), $scope.currentPage = 1;
                        }, $scope.onOrderChange = function () {
                            return $scope.select(1), $scope.currentPage = 1;
                        }, $scope.search = function () {
                            return $scope.filteredList = $filter("filter")($scope.orders, $scope.searchKeywords), $scope.onFilterChange();
                        }, $scope.order = function (rowName) {
                            return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredList = $filter("orderBy")($scope.orders, rowName), $scope.onOrderChange()) : void 0;
                        }, $scope.numPerPageOpt = [3, 5, 10, 20], $scope.numPerPage = $scope.numPerPageOpt[2], $scope.currentPage = 1, $scope.currentPageList = [], (init = function () {
                            return $scope.search(), $scope.select($scope.currentPage);
                        }), $scope.search();
                    },
                    function handleError(error) {
                        return function () {
                        };
                    });
                ngDialog.closeAll();
                },
                function handleError(error) {
                    ngDialog.closeAll();
                });
        };        
    });