angular.module("app.invoice",[])
    .config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('app.invoice', {
            url: '/invoice',
            abstract: true,
            data: {
                roles: ['ADMIN','ACCOUNTANT']
            }
        })
        .state('app.invoice.all', {
            url: '/list',
            params:{
                mode:'list'
            },
            views : {
                "@": {
                    templateUrl: "app/views/invoice/list.html",
                    controller:'InvoiceCtrl'
                }
            },
            data: {
                roles: ['ADMIN','ACCOUNTANT']
            }
        });
});
