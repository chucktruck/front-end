angular.module("app.invoice").controller("InvoiceCtrl",
    function ($scope,$http,$location,$filter,ngDialog,$timeout, $q,$state, toaster, modalService) {

    $scope.invoices=[];
    $scope.invoice={};

    $scope.invoiceStatus = ['NEW', 'ACCOUNTS', 'SEND', 'PENDING', 'PAID', 'PAID_PARTIAL'];
        $scope.selected= {};
        // gets the template to ng-include for a table row / item
        $scope.getTemplate = function (invoice) {
            if (invoice.id === $scope.selected.id) return 'edit';
            else return 'display';
        };

        $scope.edit = function (invoice) {
            $scope.selected = angular.copy(invoice);
        };

        $scope.addExpense = function() {
            ngDialog.open({
                template: '/app/views/expense/add.html',
                className: 'ngdialog-theme-default custom-width-600',
                width:600,
                scope: $scope,
                closeByDocument: false,
                closeByEscape: false
            });
        };

        $scope.getInvoiceStatusCnt = function(status) {
            var count = 0;
            angular.forEach($scope.invoices, function(value, key){
                if(value.status === status) {
                    count++;
                }
            });
            return count;
        };        
        $scope.listExpense = function() {
            ngDialog.open({
                template: '/app/views/expense/list.html',
                className: 'ngdialog-theme-default custom-width-600',
                width:600,
                scope: $scope,
                closeByDocument: false,
                closeByEscape: false
            });
        };   

        $scope.previewInvoice = function(invoice) {
            var modalDefaults = {
                backdrop: true,
                keyboard: true,
                modalFade: true,
                templateUrl: '/app/partials/invoiceModal.html'
            };
            var modalOptions = {
                closeButtonText: 'Cancel',
                actionButtonText: 'Generate PDF',
                headerText: 'Invoice',
                bodyText: '',
                order: {},
                broker: {},
                expense: {},
            };
            modalOptions.order = invoice.order;
            modalOptions.tenant = invoice.tenant;
            modalOptions.broker = invoice.order.broker;
            modalOptions.expense.orderRate = invoice.rate;
            modalOptions.expense.fuelSurcharges = invoice.fuelSurcharges;

            $http.get('/api/invoice/'+invoice.id+'/report').then(
                    function handleSuccess(res) {
                        modalOptions.invoiceData = res.data;
                        modalService.showModal(modalDefaults, modalOptions)
                        .then(function (result) {
                                //toaster.pop('success',"","Invoice Generated.");
                                $http.get('/api/invoice/'+invoice.id+'/getpdf', { responseType: 'arraybuffer' }).then(
                                function handleSuccess(res) {
                                    var file = new Blob([res.data], {type: 'application/pdf'});
                                    var fileURL = URL.createObjectURL(file);
                                    window.open(fileURL);                                   
                                     toaster.pop('success',"Invoice Generated");
                                },
                                function handleError(error) {
                                    toaster.pop('error',"","Problem Generating Invoice");
                                });
                        },
                        function handleError(error) {
                            toaster.pop('error',"","Unable to Generate Invoice.");
                            $scope.reset();
                        });            
            });
        };             
        $scope.delete = function (invoice,idx) {
            if (confirm("Do you want to delete ? Please confirm.")) {

                console.log("Deleting Invoice");
                return $http.post('/api/invoice/delete', invoice ).then(
                    function handleSuccess(res) {
                        $scope.invoices.splice(idx, 1);
                        $scope.search();
                        $scope.reset();

                    },
                    function handleError(error) {
                        toaster.pop('error',"","something wrong happened !! Please report to Admin");
                        $scope.reset();
                    });
            }
        };

        $scope.payInvoice = function(invoice, idx) {
            $scope.selectedInvoice = angular.copy(invoice);
            ngDialog.open({
                template: '/app/views/checkDetail/list.html',
                className: 'ngdialog-theme-default custom-width-600',
                controller:'CheckDetailCtrl',
                scope: $scope,
                closeByDocument: false,
                closeByEscape: true
            });          
        };

        $scope.update = function (idx) {
            var modalDefaults = {
                backdrop: true,
                keyboard: true,
                modalFade: true,
                templateUrl: '/app/partials/invoice-check.html'
            };
            var modalOptions = {
                closeButtonText: 'Close',
                actionButtonText: 'OK',
                checkInformation:  {}
            };            
            
            modalService.showModal(modalDefaults, modalOptions).then(
                function handleSuccess(result) {
                    return $http.post('/api/invoice/update', $scope.selected ).then(
                        function handleSuccess(res) {
                            $scope.invoices[idx] = angular.copy($scope.selected);
                            toaster.pop('success', '', "Invoice updated");
                            $scope.search();
                            $scope.reset();

                        },
                        function handleError(error) {
                            toaster.pop('error',"","something wrong happened !! Please report to Admin");
                            $scope.reset();
                        });                        
                        toaster.pop('success', "UnAssign Driver", "Driver Unassigned Successfully.");
                    },
                function handleError(error) {
                    toaster.pop('error',"","something wrong happened !! Please report to Admin");
                        //$scope.reset();
                });       
        };

        $scope.reset = function () {
            $scope.selected = {};
        };


        var self = $scope;

// list of `state` value/display objects
        self.states        = loadAll();
        self.selectedStateItem  = null;
        self.searchText    = null;
        self.querySearch   = querySearch;

        $scope.ok = function () {
            //$scope.company.state=this.selectedStateItem.value;
            return $http.post('/api/invoice/save', $scope.company).then(
                function handleSuccess(res) {
                    $scope.invoices.push(res.data);
                    $scope.search();
                    toaster.pop('success',"","New Invoice Created");
                    $scope.invoiceArray.push(res.data);
                },
                function handleError(error) {
                    toaster.pop('error',"","something wronng happened !!");
                });
        };
        $scope.showFilteredInvoices = function(invoice) {
            if(!$scope.invoiceStatusFilter) {
                $scope.invoiceStatusFilter = '[OPEN, NEW, PAID_PARTIAL]';
            }
            if ($scope.invoiceStatusFilter.indexOf(invoice.status) != -1)
                return invoice;
        };

        $scope.filterInvoiceByStatus = function(status) {
            $scope.invoiceStatusFilter = status;
        };

        $scope.addInvoice = function() {
            ngDialog.open({
                template: '/app/views/invoice/add.html',
                className: 'ngdialog-theme-default custom-width-600',
                width:600,
                scope: $scope,
                closeByDocument: false,
                closeByEscape: false
            });
        };
        $http.get('/api/invoices', $scope).then(
            function handleSuccess(res) {
                $scope.invoices=res.data;
                return $scope.invoices, $scope.searchKeywords = "", $scope.filteredList = [], $scope.row = "", $scope.select = function (page) {
                    var end, start;
                    return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageList = $scope.filteredList.slice(start, end);
                }, $scope.onFilterChange = function () {
                    return $scope.select(1), $scope.currentPage = 1, $scope.row = "";
                }, $scope.onNumPerPageChange = function () {
                    return $scope.select(1), $scope.currentPage = 1;
                }, $scope.onOrderChange = function () {
                    return $scope.select(1), $scope.currentPage = 1;
                }, $scope.search = function () {
                    return $scope.filteredList = $filter("filter")($scope.invoices, $scope.searchKeywords), $scope.onFilterChange();
                }, $scope.order = function (rowName) {
                    return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredList = $filter("orderBy")($scope.invoices, rowName), $scope.onOrderChange()) : void 0;
                }, $scope.numPerPageOpt = [3, 5, 10, 20], $scope.numPerPage = $scope.numPerPageOpt[2], $scope.currentPage = 1, $scope.currentPageList = [], (init = function () {
                    return $scope.search(), $scope.select($scope.currentPage);
                }), $scope.search();
            },
            function handleError(error) {
                return function () {
                };
            });



        // ******************************
        // Internal methods
        // ******************************

        /**
         * Search for states... use $timeout to simulate
         * remote dataservice call.
         */
        function querySearch (query) {
            var results = query ? self.states.filter( createFilterFor(query) ) : self.states;
            var deferred = $q.defer();
            $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
            return results;
        }

        /**
         * Build `states` list of key/value pairs
         */
        function loadAll() {
            var allStates = 'Alabama, Alaska, Arizona, Arkansas, California, Colorado, Connecticut, Delaware,Florida, Georgia, Hawaii, Idaho, Illinois, Indiana, Iowa, Kansas, Kentucky, Louisiana, Maine, Maryland, Massachusetts, Michigan, Minnesota, Mississippi, Missouri, Montana, Nebraska, Nevada, New Hampshire, New Jersey, New Mexico, New York, North Carolina,North Dakota, Ohio, Oklahoma, Oregon, Pennsylvania, Rhode Island, South Carolina,South Dakota, Tennessee, Texas, Utah, Vermont, Virginia, Washington, West Virginia,Wisconsin, Wyoming';

            return allStates.split(/, +/g).map( function (state) {
                return {
                    value: state.toLowerCase(),
                    display: state
                };
            });
        }

        /**
         * Create filter function for a query string
         */
        function createFilterFor(query) {
            var lowercaseQuery = angular.lowercase(query);

            return function filterFn(state) {
                return (state.value.indexOf(lowercaseQuery) === 0);
            };

        }
});
