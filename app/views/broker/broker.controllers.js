
angular.module("app.broker")
    .controller("BrokerCtrl",
        function ($scope,$http,$location,$filter,ngDialog,toaster, statesService) {
            var init;
            console.log($location);
            $scope.brokers=[];
            $scope.broker={};
            $scope.selected= {};

            $scope.broker.country = "US";
            // gets the template to ng-include for a table row / item
            $scope.getTemplate = function (broker) {
                if (broker.id === $scope.selected.id) return 'edit';
                else return 'display';
            };

            $scope.editBroker = function (broker) {
                $scope.selected = angular.copy(broker);
            };


            $scope.deleteBroker = function (broker,idx) {
                if (confirm("Do you want to delete ? Please confirm.")) {

                console.log("Deleting broker");
                return $http.post('/api/broker/delete', broker ).then(
                    function handleSuccess(res) {
                        $scope.brokers.splice(idx, 1);
                        $scope.search();
                        $scope.reset();

                    },
                    function handleError(error) {
                        toaster.pop('error',"","something wrong happened !! Please report to Admin");
                        $scope.reset();
                    });
                }
            };

            $scope.updateBroker = function (idx) {
                console.log("Saving broker");
                return $http.post('/api/broker/update', $scope.selected ).then(
                    function handleSuccess(res) {
                        $scope.brokers[idx] = angular.copy($scope.selected);
                        $scope.search();
                        $scope.reset();

                    },
                    function handleError(error) {
                        toaster.pop('error',"","something wrong happened !! Please report to Admin");
                        $scope.reset();
                    });


            };

            $scope.reset = function () {
                $scope.selected = {};
            };

            $scope.ok = function () {
                 return $http.post('/api/broker/save', $scope.broker ).then(
                    function handleSuccess(res) {
                        $scope.brokers.push(res.data);
                        $scope.search();
                        $scope.broker = {};
                        toaster.pop('success',"","New Broker Created");
                        if($scope.brokerArray) {
                            $scope.brokerArray.push(res.data);
                        }
                    },
                    function handleError(error) {
                        toaster.pop('error',"","something wrong happened !! Please report to Admin");
                    });
            };
            $scope.addBroker = function() {
                ngDialog.open({
                    template: '/app/views/broker/add.html',
                    className: 'ngdialog-theme-default custom-width-600',
                    width:600,
                    scope: $scope,
                    closeByDocument: false,
                    closeByEscape: false
                });
            };
            $scope.getStates = function() {
                return statesService.getUSStates();
            };
            $http.get('/api/broker/list', $scope).then(
                function handleSuccess(res) {
                    $scope.brokers=res.data;
                    return $scope.brokers, $scope.searchKeywords = "", $scope.filteredBrokers = [], $scope.row = "", $scope.select = function (page) {
                        var end, start;
                        return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageBrokers = $scope.filteredBrokers.slice(start, end);
                    }, $scope.onFilterChange = function () {
                        return $scope.select(1), $scope.currentPage = 1, $scope.row = "";
                    }, $scope.onNumPerPageChange = function () {
                        return $scope.select(1), $scope.currentPage = 1;
                    }, $scope.onOrderChange = function () {
                        return $scope.select(1), $scope.currentPage = 1;
                    }, $scope.search = function () {
                        return $scope.filteredBrokers = $filter("filter")($scope.brokers, $scope.searchKeywords), $scope.onFilterChange();
                    }, $scope.order = function (rowName) {
                        return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredBrokers = $filter("orderBy")($scope.brokers, rowName), $scope.onOrderChange()) : void 0;
                    }, $scope.numPerPageOpt = [3, 5, 10, 20], $scope.numPerPage = $scope.numPerPageOpt[2], $scope.currentPage = 1, $scope.currentPageBrokers = [], (init = function () {
                        return $scope.search(), $scope.select($scope.currentPage);
                    }), $scope.search();
                },
                function handleError(error) {
                    return function () {
                    };
                });
    });
    




