angular.module("app.broker",[])
    .config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('app.broker', {
            url: '/broker',
            abstract: true

        })

        .state('app.broker.all', {
            url: '/list',
            params:{
                mode:'list'
            },
            views : {
                "@": {
                    templateUrl: "app/views/broker/list.html",
                    controller:'BrokerCtrl'
                }
            }

        });
});
