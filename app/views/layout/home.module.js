angular.module('app.home', ['ui.router'])
    .config(function ($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('home', {
                url: '/home',
                templateUrl: "home.html",
                controller: 'HomeCtrl',
                data: {
                    roles: []
                }

            });
    });