
angular.module('app.layout')
    .controller('LayoutCtrl',function ($rootScope,$scope) {

        $scope.currentUser=angular.fromJson(localStorage.getItem("user.identity"));
        $scope.menuItems = ['Order', 'Broker', 'Company', 'Equipment'];
        $scope.activeMenu = $scope.menuItems[0];

        $scope.setActive = function(menuItem) {
            $scope.activeMenu = menuItem
        }
    });



