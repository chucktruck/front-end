angular.module("app.order",[])
    .config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('app.order', {
            url: '/order',
            abstract: true,
            data: {
                roles: ['ADMIN','DISPATCHER']
            }
        })

        .state('app.order.all', {
            url: '/list',
            params:{
                mode:'list'
            },
            views : {
                "@": {
                    templateUrl: "app/views/order/list.html",
                    controller:'OrderCtrl'
                }
            },
            data: {
                roles: ['ADMIN','DISPATCHER']
            }
        });
});
