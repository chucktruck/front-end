
angular.module("app.order")
    .controller("OrderCtrl",
        function ($scope,$http,$location,$filter,ngDialog,$timeout, $q,$state,toaster, modalService, $interval, $timeout, $rootScope) {
        var init;

        $rootScope.selectedEntryPath = $location.path();
        $scope.openLegDetails = function(order) {
            $scope.selectedOrderId = order.orderId;
            $scope.activeLeg = order.legs[0].id;
                $http.get('/api/order/get/'+$scope.selectedOrderId, $scope).then(
                    function handleSuccess(res) {            
                        $scope.selectedOrder = res.data;
                        $scope.orderLegs = Array.prototype.slice.call($scope.selectedOrder.legs); //making shalow copy of arra
                        $scope.orderLegs.reverse();


                        ngDialog.open({
                            template: '/app/views/order/showleg.html',
                            className: 'ngdialog-theme-default custom-width-800',
                            controller: 'OrderCtrl',
                            scope: $scope,
                            closeByDocument: false,
                            closeByEscape: true
                        });
                    });
        };

        $scope.showSelectedLeg = function(leg) {
            $scope.selectedLeg = {};
            $scope.activeLeg = leg.id;
            $scope.selectedLeg.id = leg.id;
            $scope.selectedLeg.legType = leg.legType;
            $scope.selectedLeg.terminal = leg.terminals[0];
            $scope.selectedLeg.consignees = leg.consignees;
            $scope.selectedLeg.driver = leg.driver;
            $scope.selectedLeg.status = leg.status;
            $scope.selectedLeg.equipments = [];

            var equipment = {}
            equipment.type = "CONTAINER";
            equipment.number = leg.containerNumber;
            equipment.dropLocation = leg.containerDropLocation;
            // equipment.dueDate = leg.order.containerLastFreeDate ? leg.order.containerLastFreeDate : '-'
            equipment.status = leg.containerStatus ? leg.containerStatus : '-'
            
            $scope.selectedLeg.equipments.push(equipment);
            
            equipment = {}
            equipment.type="CHASSIS";
            equipment.number = leg.chassisNumber;
            equipment.dropLocation = leg.chassisDropLocation;
            // equipment.dueDate = leg.order.containerLastFreeDate ? leg.order.containerLastFreeDate : '-'
            equipment.status = leg.chassisStatus ? leg.chassisStatus : '-'

            $scope.selectedLeg.equipments.push(equipment);


                var URL = '/api/legs/nextstatus';
                var consigneeId = 0;

                if(leg.status === 'TRANSIT') {
                    angular.forEach(leg.consignees, function(consignee) {
                        if(consignee.status !== 'DELIVERED'){
                            consigneeId = consignee.id;
                            return;
                        }
                    });
                } else  {                    
                    $scope.selectedLeg.nextstatus = leg.nextstatus;
                }
                $http.get(URL, {
                    params: {
                        legId: leg.id,
                        consigneeId: consigneeId
                    }
                }).then (function handleSuccess(res) {
                    $scope.selectedLeg.nextstatus = res.data;
                }, function handleError(error){
                    toaster.pop('error', 'Next Status', 'Unable to get next Status');
                });
        };
        
        // $scope.socket = {
        //     client : null
        // };
        
        $scope.showPopOver = function() {
            $('[data-toggle="popover"]').popover({trigger: "hover", html: true});
        };

        // $scope.socket = function(){
        //     console.log('Updating Order list');
        //     $interval(function() {
        //         $rootScope.$broadcast('updateOrderList');
        //     }, 15*1000);
        // }();

        // $scope.initSocket = function () {
        //     $scope.socket.client = new WebSocket('wss://pure-meadow-29899.herokuapp.com/notifications');
        //     $scope.socket.client.onmessage = function(message) {
        //         console.log(message);
        //         if(message.data.indexOf('UpdateLegStatus') != -1) {
        //             var statusInfo = message.data.split(' ');
        //             toaster.pop('success',"", "Status for Order "+statusInfo[1] + " changed to " + statusInfo[3] );
        //             $rootScope.$broadcast('updateOrderList');
        //         }
        //     };
        //     $scope.socket.client.onOpen = function(){
        //         console.log('Connection Open');
        //         $scope.socket.client.send('HELLO');
        //     };
        //     $scope.socket.client.onError = function() {
        //         console.log('Error');
        //     }
        //     $scope.socket.client.onClose = function(){
        //         console.log('Connection Close');
        //         $timeout(function() {
        //             $scope.initSocket();
        //         }, 1000);
        //     }
        // }();

        $scope.orderCharges = [];

        $scope.orders=[];
        $scope.dispatchOrder={};
        $scope.selected= {};
        $scope.legStatus = ['NEW', 
                            'ACCEPTED', 
                            'REJECTED', 
                            'YARD_PULL',
                            'CONSIGNEE',
                            'TRANSIT',
                            'CHECK_IN',
                            'DETENTION_START',
                            'DETENTION_END',
                            'CHECK_OUT', 
                            'DELIVERED',
                            'YARD_IN',
                            'TERMINAL',
                            'DONE',
                            'COMPLETE'];

        $scope.dispatchOrder.legs = {};
        $scope.dispatchOrder.orderRate = 0;
        $scope.dispatchOrder.fuelCharges = 0;
        $scope.dispatchOrder.extraCharges = 0;
        
        $scope.workspaces = [
            { id: 1, 
                title:"Consignee 1", 
                name: "", 
                selectedItem: 'Consignee Name', 
                appointment: '', 
                isOpenTime:'',
                deliveryNotes: '',
                puNumber: '',
                openTimeAppointment: '',
                refNumber: '',
                searchItem:'searchConsigneeText', active:true  }
        ];
        
        $scope.addWorkspace = function () {
            setAllInactive();
            addNewWorkspace();
        }; 
        
        $scope.addWorkspace = function () {
            if($scope.workspaces.length < 3){
                setAllInactive();
                addNewWorkspace();
            }else{
                $scope.workspaces[0].active = true;
                $scope.errorMessage = 'Cannot add more then 3 Consignee';
            }
        };
        
        $scope.removeCurrentTab= function(id) {
            if(id == 1) {
            } else {
                setAllInactive();
                for(var i = 0 ; i < $scope.workspaces.length ; i ++ ){
                    if($scope.workspaces[i].id == id){
                    $scope.workspaces.splice(i, 1);
                    $scope.workspaces[i-1].active = true;
                    }
                }
            }
        };

        var setAllInactive = function() {
            angular.forEach($scope.workspaces, function(workspace) {
                workspace.active = false;
            });
        };

        var addNewWorkspace = function() {
            var id = $scope.workspaces.length + 1;
            //var getStringValue = numToStringMap[id];
            $scope.workspaces.push({
                id: id,
                title: "Consignee "+id, 
                name: '',
                appointment: '',
                isOpenTime: '',
                openTimeAppointment: '',
                deliveryNotes: '',
                puNumber: '',
                refNumber: '',
                searchItem: 'searchConsigneeText',
                active: true
            });
        };        
        
        $scope.removeCurrrentTab = function() {
            $scope.workspaces.splice(item, 1);
        };

        $scope.toggleAdvancedSearch = function() {
            $scope.isColumnwiseSearchEnabled = !$scope.isColumnwiseSearchEnabled;
        }        

        var self = $scope;
        
        self.brokerArray= loadAllBrokers();

        self.companyArray = loadAllCompany();

        self.equipmentArray = loadAllEquipment();

        $scope.isBrokerNumberExists = function() {
            var item = document.getElementById('txtBroker').value;
            var brokerId = document.querySelector("#brokers option[value='"+ item +"']").dataset.id;
            var brokerOrderNum = $scope.dispatchOrder.brokerOrderNumber;
            $http.get('/api/broker/'+brokerId+'/exists/brokernumber/'+brokerOrderNum).then(
                function handleSuccess(result) {
                    if(result.data === true) {
                        alert('Load/Order already exists for this Broker Order Number');
                        $scope.dispatchOrder.brokerordernumber = '';
                    }
                }, function handleError(error) {
                    toaster.pop('error',"","Error creating New Order");
                });
        };

        $scope.closeDialog = function() {
            $scope.theModal.dismiss('cancel');
        };
     
        /* Validations */
        $scope.isBrokerSelectedFromDataList = function() {
            var item = document.getElementById('txtBroker').value;
            if(item.length > 0) {
                var broker = $scope.brokerArray.filter(function(b) { return b.name.toLowerCase() === item.toLowerCase() })[0];
                if (broker) {
                    $scope.brokerInformation = `${broker.street} ${broker.city}, ${broker.state}.`;
                } else {
                    $scope.brokerInformation = "Broker Not Found. Please select from list or add new.";
                    document.getElementById('txtBroker').value = '';
                }
            }
        };

        $scope.isShipperSelectedFromDataList = function() {
            var item = document.getElementById('txtTerminal').value;
            if(item.length > 0) {
                var company = $scope.companyArray.filter(function(b) { return b.name.toLowerCase() === item.toLowerCase() })[0];
                if (company) {
                    $scope.shipperInformation = `${company.street} ${company.city}, ${company.state}.`;
                    $scope.dropLocation = `Drop Location: ${company.street}, ${company.city}. ${company.state}`;
                } else {
                    $scope.shipperInformation = "Shipper/Terminal Not Found. Please select from list or add new.";
                    document.getElementById('txtTerminal').value = '';
                }
            }
        };

        $scope.isContainerSelectedFromDataList = function() {
            var item = document.getElementById('txtContainerType').value;
            if(item.length > 0) {
                var equipment = $scope.equipmentArray.filter(function(b) { 
                    if(b.name) {
                        return b.name.toLowerCase() === item.toLowerCase();
                    }
                })[0];
                if (equipment) {
                    $scope.containerInformation = `FREE DAYS: ${equipment.freeDays}
                        -- LAST FREE DATE: ${moment($scope.dispatchOrder.rampAppointment).add(3, 'days').format('YYYY-MM-DD')}`;
                    $scope.dropLocation = `DROP LOCATION: ${equipment.dropLocation}`;
                } else {
                    $scope.containerInformation = "Container Not Found. Please select from list or add new.";
                    document.getElementById('txtContainerType').value = '';
                }
            }
        };
        
        $scope.isChassisSelectedFromDataList = function() {
            var item = document.getElementById('txtChassisType').value;
            if(item.length > 0) {
                var equipment = $scope.equipmentArray.filter(function(b) { 
                    if(b.company) {
                        return b.company.toLowerCase() === item.toLowerCase();;
                    }
                })[0];    
                if (equipment) {
                    $scope.chassisInformation = `Rent Per Day: $ ${equipment.rent}.`;
                } else {
                    $scope.chassisInformation = "Chassis Not Found. Please select from list or add new.";
                    document.getElementById('txtChassisType').value = '';
                }
            }
        };        


        $scope.ok = function () {
            var item = document.getElementById('txtBroker').value;
            $scope.dispatchOrder.brokerId = document.querySelector("#brokers option[value='"+ item +"']").dataset.id;

            item = document.getElementById('txtTerminal').value;
            $scope.dispatchOrder.rampId = document.querySelector("#terminals option[value='"+ item +"']").dataset.id;

            item = document.getElementById('txtContainerType').value;
            if(item) {
                $scope.dispatchOrder.containerId = document.querySelector("#containers option[value='"+ item +"']").dataset.id;    
            }

            item = document.getElementById('txtChassisType').value;
            if(item) {
                $scope.dispatchOrder.chassisId = document.querySelector("#chassis option[value='"+ item +"']").dataset.id;    
            }
            
            $scope.dispatchOrder.consigneeId = [];
            $scope.dispatchOrder.consigneeAppointment = [];
            $scope.dispatchOrder.consigneePUNumber = [];
            $scope.dispatchOrder.consigneeNotes = [];
            $scope.dispatchOrder.isConsigneeOpenTime = [];
            $scope.dispatchOrder.consigneeOpenTimeAppointment = [];
            $scope.dispatchOrder.consigneeReferenceNumber = [];

            angular.forEach(this.workspaces, function(workspace, ind) {
                item = workspace.name;
                $scope.dispatchOrder.consigneeId.push(document.querySelector("#consignees option[value='"+ item +"']").dataset.id);
                $scope.dispatchOrder.consigneeAppointment.push(workspace.selectedDate);
                $scope.dispatchOrder.consigneePUNumber.push(workspace.puNumber);
                $scope.dispatchOrder.consigneeNotes.push(workspace.deliveryNotes);
                $scope.dispatchOrder.isConsigneeOpenTime.push(workspace.isOpenTime == "True" ? true: false);
                $scope.dispatchOrder.consigneeOpenTimeAppointment.push(workspace.openTimeAppointment);
                $scope.dispatchOrder.consigneeReferenceNumber.push(workspace.refNumber);
            });
            $scope.dispatchOrder.isRampOpenTime = $scope.dispatchOrder.isRampOpenTime == "True" ? true: false;
            $scope.dispatchOrder.isChassisSplit = $scope.dispatchOrder.isChassisSplit == "True" ? true: false;
            $scope.dispatchOrder.orderTotal= $scope.dispatchOrder.orderRate * 1 + 
                                                $scope.dispatchOrder.fuelCharges * 1 + $scope.dispatchOrder.extraCharges * 1;

            $scope.dispatchOrder.expenses = JSON.stringify($scope.orderCharges);
            
            return $http.post('/api/order/save', $scope.dispatchOrder).then(
                function handleSuccess(res) {
                    $scope.orders.push(res.data);
                    toaster.pop('success',"","New Order created successfully.");
                    $scope.search();
                    $scope.dispatchOrder={};
                    $rootScope.$broadcast('updateOrderList');
                    },
                function handleError(error) {
                    toaster.pop('error',"","Error creating New Order");
                    ngDialog.closeAll();                                
            });
        };

        $http.get('/api/order/list', $scope).then(
            function handleSuccess(res) {
                $scope.orders=res.data;
                return $scope.orders, $scope.searchKeywords = "", $scope.filteredOrders = [], $scope.row = "", $scope.select = function (page) {
                    var end, start;
                    return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageOrders = $scope.filteredOrders.slice(start, end);
                }, $scope.onFilterChange = function () {
                    return $scope.select(1), $scope.currentPage = 1, $scope.row = "";
                }, $scope.onNumPerPageChange = function () {
                    return $scope.select(1), $scope.currentPage = 1;
                }, $scope.onOrderChange = function () {
                    return $scope.select(1), $scope.currentPage = 1;
                }, $scope.search = function () {
                    return $scope.filteredOrders = $filter("filter")($scope.orders, $scope.searchKeywords), $scope.onFilterChange();
                }, $scope.order = function (rowName) {
                    return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredOrders = $filter("orderBy")($scope.orders, rowName), $scope.onOrderChange()) : void 0;
                }, $scope.numPerPageOpt = [3, 5, 10, 20], $scope.numPerPage = $scope.numPerPageOpt[2], $scope.currentPage = 1, $scope.currentPageOrders = [], (init = function () {
                    return $scope.search(), $scope.select($scope.currentPage);
                }), $scope.search();
            },
            function handleError(error) {
                return;
        });


         $scope.numItemsPerPage = [3, 5, 10, 20];
         $scope.pageSize = $scope.numItemsPerPage[2];

        $scope.getLegCount =  function(order) {
            return order.legs.length;
        };

        $scope.getOrderStatusCnt = function(status) {
            var count = 0;
            angular.forEach($scope.orders, function(value, key){
                if(value.status === status) {
                    count++;
                }
            });
            return count;
        };

        $scope.getContainerCount = function(status) {
            var count = 0;
            angular.forEach($scope.orders, function(value, key){
                if(value.containerStatus === status) {
                    count++;
                }
            });
            return count;
        };

        $scope.getChassisCount = function(status) {
            var count = 0;
            angular.forEach($scope.orders, function(value, key){
                if(value.chassisStatus === status) {
                    count++;
                }
            });
            return count;
        };

        $scope.addBroker = function() {
            ngDialog.open({
                template: '/app/views/broker/add.html',
                className: 'ngdialog-theme-default custom-width-600',
                controller:'BrokerCtrl',
                scope: $scope,
                closeByDocument: false,
                closeByEscape: false            
            });
        };

        $scope.addCompany = function() {
            ngDialog.open({
                template: '/app/views/company/add.html',
                className: 'ngdialog-theme-default custom-width-600',
                controller:'CompanyCtrl',
                scope: $scope,
                closeByDocument: false,
                closeByEscape: false
            });
        };


        $scope.addEquipment = function() {
            ngDialog.open({
                template: '/app/views/equipment/add.html',
                className: 'ngdialog-theme-default custom-width-600',
                controller:'EquipmentCtrl',
                scope: $scope,
                closeByDocument: false,
                closeByEscape: false
            });
        };

        $scope.addOrder = function() {
            ngDialog.open({
                template: '/app/views/order/add.html',
                className: 'ngdialog-theme-default custom-width-800',
                controller: 'OrderCtrl',
                scope: $scope,
                closeByDocument: false,
                closeByEscape: true
            });
        };

        $scope.addExpense = function(order, idx) {
            $scope.selectedOrder = angular.copy(order);
            ngDialog.open({
                template: '/app/views/expense/list.html',
                className: 'ngdialog-theme-default custom-width-600',
                controller:'ExpenseCtrl',
                scope: $scope,
                closeByDocument: false,
                closeByEscape: true
            });          
        };

        $scope.addOrderCharges = function (order) {
            $scope.selectedOrder = angular.copy(order);
            ngDialog.open({
                template: '/app/views/expense/charges.html',
                className: 'ngdialog-theme-default custom-width-600',
                controller:'ExpenseCtrl',
                scope: $scope,
                closeByDocument: false,
                closeByEscape: true             
            });          
        };

        $scope.columnFilter = function(order) {
            return order;
        };

        $scope.showFilteredOrders = function(order) {
            console.log(order.orderId);
            if(!$scope.orderStatusFilter) {
                $scope.orderStatusFilter = '[OPEN, NEW, COVERED, ASSIGNED, YARD_PULL, YARD_IN]';
            }
            if ($scope.orderStatusFilter.indexOf(order.status) != -1) {
                console.log("SelectedOrder: "+ order.orderId);
                return order;
            }
        };

        $scope.filterOrderByStatus = function(status) {
            $scope.orderStatusFilter = status;
            $rootScope.$broadcast('updateOrderList');
        };

        $scope.showScheduleOrders = function (scheduleOrder) {
            $scope.orders=scheduleOrder.dispatchOrder;
            return $scope.orders, $scope.searchKeywords = "", $scope.filteredList = [], $scope.row = "", $scope.select = function (page) {
                var end, start;
                return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, 
                $scope.currentPageList = $scope.filteredList.slice(start, end);
            }, $scope.onFilterChange = function () {
                return $scope.select(1), $scope.currentPage = 1, $scope.row = "";
            }, $scope.onNumPerPageChange = function () {
                return $scope.select(1), $scope.currentPage = 1;
            }, $scope.onOrderChange = function () {
                return $scope.select(1), $scope.currentPage = 1;
            }, $scope.search = function () {
                return $scope.filteredList = $filter("filter")($scope.orders, $scope.searchKeywords), $scope.onFilterChange();
            }, $scope.order = function (rowName) {
                return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredList = $filter("orderBy")($scope.orders, rowName), $scope.onOrderChange()) : void 0;
            }, $scope.numPerPageOpt = [3, 5, 10, 20], $scope.numPerPage = $scope.numPerPageOpt[2], $scope.currentPage = 1, $scope.currentPageList = [], (init = function () {
                return $scope.search(), $scope.select($scope.currentPage);
            }), $scope.search();

        }; 

        function loadAllEquipment() {
            $http.get('/api/equipment/list', $scope).then(
                function handleSuccess(res) {
                    self.equipmentArray=res.data;
                },
                function handleError(error) {
                    return function () {
                    };
                });
        }

        function loadAllBrokers() {
            $http.get('/api/broker/list', $scope).then(
                function handleSuccess(res) {
                    self.brokerArray = res.data;
                },
                function handleError(error) {
                    return function () {
                    };
                });
        }

        function loadAllCompany() {
            $http.get('/api/company/list', $scope).then(
                function handleSuccess(res) {
                    self.companyArray=res.data;
                },
                function handleError(error) {
                    return function () {
                    };
                });
        }

        /**
         * Create filter function for a query string
         */
        function createFilterFor(query) {
            var lowercaseQuery = angular.lowercase(query);

            return function filterFn(filterObject) {
                return (angular.lowercase(filterObject.name).indexOf(lowercaseQuery) === 0);
            };
        }
        
        $scope.editOrder = function (order) {
            $scope.edit = angular.copy(order);
            $scope.edit.brokerName = order.broker.name;
            $scope.edit.containerType = order.containerType ? order.containerType.name : null;
            $scope.edit.chassisType = order.chassisCompany ? order.chassisCompany.name : null;
            ngDialog.open({
                template: '/app/views/order/edit.html',
                className: 'ngdialog-theme-default custom-width-800',
                data: $scope.edit,
                controller: 'OrderCtrl',
                appointment: $scope.editDate,
                closeByDocument: false,
                closeByEscape: true
            });
        };

        $scope.editLeg = function (leg) {
            $scope.selectedLeg = angular.copy(leg);
        };

        $scope.updateLegStatus = function (status, leg) {
            var URL = '/api/legs/nextstatus';
            var consigneeId = 0;
            var tenantId = leg.driver.tenant.tenantId;
            var data = { 
                tenantId: tenantId,
                driverPhone: leg.driver.phone,
                newStatus: status.command
                };
            $http({
                url: '/api/order/' + $scope.selectedOrderId+'/leg/'+leg.id+'/changestatus', 
                method: "POST",
                params: data
            }).then(
                function handleSuccess(res) {
                    toaster.pop('success', "Change Status", "Consignee Status updated Successfully.");
                },
                function handleError(error) {
                    toaster.pop('error',"","something wrong happened !! Please report to Admin");
                });
            this.closeThisDialog();
            $rootScope.$broadcast('updateOrderList');
            //$rootScope.$broadcast('updateEquipmentStatus');
            //$rootScope.$broadcast('updateAccountingStatus');
        };

        $scope.unassignDriver = function(leg) {
            if (confirm("Do you want to un-assign driver " + leg.driver.name.toUpperCase() +"?. Please confirm.")) {
                $http.post('/api/leg/'+leg.id+'/unassign/driver/'+leg.driver.id, {})
                .then(
                function handleSuccess(res) {
                    toaster.pop('success', "UnAssign Driver", "Driver Unassigned Successfully.");
                    this.closeThisDialog();
                },
                function handleError(error) {
                    toaster.pop('error',"","something wrong happened !! Please report to Admin");
                    this.closeThisDialog();
                });       
            }
        };

        $scope.addOrderLeg = function(order, idx) {
            $scope.selectedOrder = angular.copy(order);
            console.log($scope);
            ngDialog.open({
                template: '/app/views/transaction/addLeg.html',
                className: 'ngdialog-theme-default custom-width-600',
                controller:'OrderTransactionCtrl',
                scope: $scope,
                closeByDocument: false,
                closeByEscape: false
            });          
        };

        $scope.deleteOrder = function (order,idx) {
              if (confirm("Do you want to delete  Order# " + idx +" ? Please confirm.")) {
                console.log("DELETING ORDER");
                $http.post('/api/order/delete', order ).then(function handleSuccess(res) {
                    toaster.pop('success',"Delete Order","Order Deleted Successfully.");
                    $scope.orders.splice(idx, 1);
                    $scope.search();
                    $scope.reset();
                    $scope.showAllOrders(); // Need to come up with better solution
                },
                function handleError(error) {
                    toaster.pop('error',"","something wrong happened !! Please report to Admin");
                    $scope.reset();
                });
            }
        };

        $scope.selectedIdx = -1;
        $scope.getAvailableDrivers =  function(leg) {

            var terminal = leg.terminal;
            var consignees = leg.consignees;
            //modalOptions.dropLocation = leg.dropLocation;
            $scope.flatDriverRate = 0;
            $scope.customDriverRate = 0;
            $scope.driverRateType = {};
            var origin;
            if(terminal.name === "YARD") {
                origin = 'Oakland CA';
            } else {
                origin = `${terminal.city.trim()} ${terminal.state.trim()}`;
            }
            var destination;
            if(consignees) {
                angular.forEach(consignees, function(consignee) {
                    if(consignee.status !== 'DELIVERED') {
                        destination = `${consignee.city.trim()} ${consignee.state.trim()}`;
                        return;
                    }
                });
            } else { //No consignee means leg is YTT
                destination = (leg.dropLocation.indexOf(',') + 1, leg.dropLocation.indexOf('.') - leg.dropLocation.indexOf(',') -1) ;
            }

            $http.get('/api/distancerate/type/'+ leg.legType + '/origin/'+ origin +'/destination/'+ destination)
            .then(
                function handleSuccess(res) {
                    $scope.legDistanceRate = res.data;
                    $http.get('/api/driver/available').then(
                        function handleSuccess(res) {
                        $scope.availableLegDrivers = res.data;

                        $scope.availableDriversDialogId = ngDialog.open({
                            template: '/app/partials/driverModal.html',
                            className: 'ngdialog-theme-default custom-width-800',
                            scope: $scope,
                            closeByDocument: false,
                            closeByEscape: true
                        });
                    }, function handleError(error) {
                        console.log("No available Drivers");
                    });
                }, function handleError(error){
                    console.log(error);
                });
        };

        $scope.selectDistanceRate = function(rate) {
            $scope.flatDriverRate = rate.rate;
        }

        $scope.selectDriverRateType = function(type) {
            $scope.driverRateType = type;
        }

        $scope.assignDriverToLeg = function(driver) {
            if(driver.rate > 0) {
                $http.post('/api/leg/'+$scope.selectedLeg.id+'/assign/driver/', driver).then(
                    function handleSuccess(res) {
                        toaster.pop('success',"Driver Assigned","Driver Assigned Successfully");

                    },
                    function handleError(error) {
                        toaster.pop('error',"","Driver Not Assigned.");
                        $scope.reset();
                });
                this.selectedLeg.driver = driver;
                this.closeThisDialog();
            } else {
                alert('Driver Rate Missing. Please provider driver rate for the leg');
            }
        }

        $scope.deleteLeg = function (leg,idx) {
         var modalOptions = {
                    closeButtonText: 'Cancel',
                    actionButtonText: 'Delete',
                    headerText: 'Delete Leg #' + leg.transId + '?',
                    bodyText: 'Are you sure you want to delete this Leg?'
                    };

                    modalService.showModal({}, modalOptions).then(
                        function (result) {
                            $http.delete('/api/delete/leg/'+leg.transId, null).then(
                                function handleSuccess(res) {
                                    //$scope.orders.splice(idx, 1);
                                    toaster.pop('success',"Delete Leg","Order Leg Deleted Successfully.");
                                    $scope.search();
                                    $scope.reset();
                                },
                                function handleError(error) {
                                    toaster.pop('error',"","something wrong happened !! Please report to Admin");
                                    //$scope.reset();
                                });
                        });            
        };

        $scope.updateOrder = function () {
            console.log("Updating the order");
            
            $scope.ngDialogData.brokerId = $scope.ngDialogData.broker.id;

            var firstLeg = $scope.ngDialogData.legs[0];
            item = document.getElementById('txtTerminal').value;
            $scope.ngDialogData.rampId = document.querySelector("#terminals option[value='"+ item +"']").dataset.id;

            item = document.getElementById('txtContainerType').value;
            if(item) {
                $scope.ngDialogData.containerId = document.querySelector("#containers option[value='"+ item +"']").dataset.id;    
            }

            item = document.getElementById('txtChassisType').value;
            if(item) {
                $scope.ngDialogData.chassisId = document.querySelector("#chassis option[value='"+ item +"']").dataset.id;    
            } 

            angular.forEach(firstLeg.terminals, function(terminal) {
                //$scope.ngDialogData.rampId = terminal.id;
                $scope.ngDialogData.rampAppointment = new Date(terminal.appointment);
                $scope.ngDialogData.rampNotes = terminal.notes;
                $scope.ngDialogData.rampPUNumber = terminal.punumber;
                $scope.ngDialogData.rampPULocation = terminal.pulocation;
                $scope.ngDialogData.isRampOpenTime = terminal.openTime == "True" ? true: false;
                $scope.ngDialogData.rampOpenTimeAppointment = terminal.rampOpenTimeAppointment;
            });

            $scope.ngDialogData.consigneeId = [];
            $scope.ngDialogData.consigneeAppointment = [];
            $scope.ngDialogData.consigneeNotes = [];
            $scope.ngDialogData.consigneePUNumber = [];
            $scope.ngDialogData.consigneeReferenceNumber = [];                        
            $scope.ngDialogData.isConsigneeOpenTime = [];            
            $scope.ngDialogData.consigneeOpenTimeAppointment = [];

            angular.forEach(firstLeg.consignees, function(con){
                $scope.ngDialogData.consigneeId.push(document.querySelector("#consignees option[value='"+ con.name +"']").dataset.id);
                $scope.ngDialogData.consigneeAppointment.push(new Date(con.appointment));
                $scope.ngDialogData.consigneeNotes.push(con.notes);
                $scope.ngDialogData.consigneePUNumber.push(con.punumber);
                $scope.ngDialogData.consigneeReferenceNumber.push(con.refNumber);
                $scope.ngDialogData.isConsigneeOpenTime.push(con.openTime == "True" ? true: false);
                $scope.ngDialogData.consigneeOpenTimeAppointment.push(con.openTimeAppointment);
            });

            if($scope.chassisCompany) {
                $scope.ngDialogData.chassisId = $scope.ngDialogData.chassisCompany.id;
            }
            if($scope.containerType) {
                $scope.ngDialogData.containerId = $scope.ngDialogData.containerType.id;
            }
            $scope.ngDialogData.isChassisSplit = $scope.ngDialogData.chassisSplit== "True" ? true: false;

            return $http.post('/api/order/edit', $scope.ngDialogData).then(
                function handleSuccess(res) {
                    toaster.pop('success',"","Order updated successfully");
                    $scope.search();
                    $scope.resetOrder();
                    $rootScope.$broadcast('updateOrderList');

                },
                function handleError(error) {
                    toaster.pop('error',"","something wrong happened !! Please report to Admin");
                    //$scope.reset();
                });
        };

        $scope.updateLeg = function (idx, leg) {
            
            var item = document.getElementById('txtTerminal').value;
            var company = document.querySelector("#companies option[value='"+ item +"']");
            var addres;
            if(company) {
                address = company.dataset.id;
                $scope.selectedLeg.origin = address;
            }
            item = document.getElementById('txtConsigneeOne').value;
            company = document.querySelector("#companies option[value='"+ item +"']");
            if(company) { 
                address = company.dataset.id;
                $scope.selectedLeg.consigneeOne = address;
            }

            item = document.getElementById('txtConsigneeTwo').value;
            company = document.querySelector("#companies option[value='"+ item +"']");
            
            if(company) {
                address = company.dataset.id;
                $scope.selectedLeg.consigneeTwo = address;
            }
            
            item = document.getElementById('txtConsigneeThree').value;
            company = document.querySelector("#companies option[value='"+ item +"']");
            if(company) {
                address = company.dataset.id;
                $scope.selectedLeg.consigneeThree = address;
            }

            item = document.getElementById('txtDropLocation').value;
            company = document.querySelector("#companies option[value='"+ item +"']");
            if (company) {
                address = company.dataset.id;
                $scope.selectedLeg.dropLocation = address;            
            }
            return $http.post('/api/order/'+$scope.orderIdExpanded+'/leg/edit', $scope.selectedLeg ).then(
                function handleSuccess(res) {
                    toaster.pop('success', "Edit Leg", "Status updated successfully.");
                    //$scope.legs[idx] = angular.copy($scope.selectedLeg);
                    $scope.resetLeg();

                },
                function handleError(error) {
                    toaster.pop('error',"","something wrong happened !! Please report to Admin");
                    //$scope.resetLeg();
                });
        };

        $scope.resetOrder = function () {
            $scope.selected = {};
        };
        $scope.resetLeg = function () {
            $scope.selectedLeg = {};
        };


        /* broadcase event */
        $scope.$on("updateOrderList",function(){
            console.log("Order Updated")
            $http.get('/api/order/list', $scope).then(
            function handleSuccess(res) {
                $scope.orders=res.data;
                return $scope.orders, $scope.searchKeywords = "", $scope.filteredOrders = [], $scope.row = "", $scope.select = function (page) {
                    var end, start;
                    return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageOrders = $scope.filteredOrders.slice(start, end);
                }, $scope.onFilterChange = function () {
                    return $scope.select(1), $scope.currentPage = 1, $scope.row = "";
                }, $scope.onNumPerPageChange = function () {
                    return $scope.select(1), $scope.currentPage = 1;
                }, $scope.onOrderChange = function () {
                    return $scope.select(1), $scope.currentPage = 1;
                }, $scope.search = function () {
                    return $scope.filteredOrders = $filter("filter")($scope.orders, $scope.searchKeywords), $scope.onFilterChange();
                }, $scope.order = function (rowName) {
                    return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredOrders = $filter("orderBy")($scope.orders, rowName), $scope.onOrderChange()) : void 0;
                }, $scope.numPerPageOpt = [3, 5, 10, 20], $scope.numPerPage = $scope.numPerPageOpt[2], $scope.currentPage = 1, $scope.currentPageOrders = [], (init = function () {
                    return $scope.search(), $scope.select($scope.currentPage);
                }), $scope.search();
            },
            function handleError(error) {
                return;
            });
        });
    })
    .filter('legTypeMappingFilter', function() {
        var map = new Map();
       
            map.set("YTT", "Yard To Terminal");
            map.set("YTC", "Yard To Consignee");
            map.set("TTY", "Terminal To Yard");
            map.set("CTY", "Consignee To Yard");
            map.set("CTT", "Consignee To Terminal");
            map.set("TTC", "Terminal To Consignee");
        
        return function(input) {
          var fullName = map.get(input);
          return fullName;
        }; 
    })
    .filter('consigneePopupFilter', function() {
      return function(input) {
          var html = '';
          for (var idx in input) {
              html += '<div>' + input[idx].name +' '+ input[idx].status+ '</div>';
              html += '<h6>' + input[idx].street +',' + input[idx].city +'.' + input[idx].state + '</h6>';
          }
          return html;
      };
    })
    .filter('consigneeCommentsFilter', function() {
      return function(input) {
          var html = '';
          for (var idx in input) {
              html += '<div>' + input[idx].notes + '</div>';
          }
          return html;
      };
    })    
    .filter('consigneeOpenTimePopFilter', function() {
      return function(input) {
          var html = '';
          for (var idx in input) {
              html += '<div>' + input[idx].openTimeAppointment + '</div>';
          }
          return html;
      };
    })
    .filter('consigneeCommentsLimitFilter', function() {
        return function(input) {
          var html = '';
          for (var idx in input) {
              html += input[idx].notes;
          }
          return html;
        };
    })    
    .filter('columnwiseSearch', function() {
        return function(data,skill,status) {
            var output = []; // store result in this
            
            /**@case1 if both searches are present**/
            if(!!skill && !!status){
                skill = skill.toLowerCase(); 
                status = status.toLowerCase();
                //loop over the original array
                for(var i = 0;i<data.length; i++){
                    // check if any result matching the search request
                    if(data[i].skill.toLowerCase().indexOf(skill) !== -1 && data[i].status.toLowerCase().indexOf(status) !== -1){
                        //push data into results array
                        output.push(data[i]);
                    }
                }
            } else if(!!skill){ /**@case2 if only skill query is present**/
                skill = skill.toLowerCase();
                for(var j = 0;j<data.length; j++){
                    if(data[j].skill.toLowerCase().indexOf(skill) !== -1){
                        output.push(data[j]);
                    }
                }
            } else if(!!status){ /**@case3 if only status query is present**/
                status = status.toLowerCase();
                for(var k = 0;k<data.length; k++){
                    if(data[k].status.toLowerCase().indexOf(status) !== -1){
                        output.push(data[k]);
                    }
                }
            } else {
                /**@case4 no query is present**/
                output = data;
            }
            return output; // finally return the result
        };
    });



