angular.module("app.bill",[])
    .config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('app.bill', {
            url: '/bill',
            abstract: true,
            data: {
                roles: ['ADMIN','ACCOUNTANT']
            }
        })
        .state('app.bill.all', {
            url: '/list',
            params:{
                mode:'list'
            },
            views : {
                "@": {
                    templateUrl: "app/views/bill/list.html",
                    controller:'BillCtrl'
                }
            },
            data: {
                roles: ['ADMIN','ACCOUNTANT']
            }
        })
        .state('app.bill.paymanager', {
            url: 'driverPayForm',
            views : {
                "@": {
                    templateUrl: "app/views/bill/driverPayForm.html",
                    controller:'BillCtrl'
                }
            },
            data: {
                roles: ['ADMIN','ACCOUNTANT']
            }            
        });
});
