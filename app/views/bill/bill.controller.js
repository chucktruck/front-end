angular.module("app.bill").controller("BillCtrl",
    function ($scope,$http,$location,$filter,ngDialog,$timeout, $q,$state) {

    $scope.bills=[];
    $scope.bill={};
    $scope.payment = {};
    $scope.driverBills = [];

    $scope.billStatus = ['NEW', 'ACCOUNTS', 'UNPAID', 'PENDING', 'PAID', 'PAID_PARTIAL'];
        $scope.selected= {};
        // gets the template to ng-include for a table row / item
        $scope.getTemplate = function (bill) {
            if (bill.id === $scope.selected.id) return 'edit';
            else return 'display';
        };

        $scope.edit = function (bill) {
            $scope.selected = angular.copy(bill);
        };

        $scope.toggleRows = function(value) {
            angular.forEach($scope.legsByDate, function(leg){
                leg.selected = $scope.allSelected;
            });
        };

        $scope.delete = function (bill,idx) {
            if (confirm("Do you want to delete ? Please confirm.")) {

                console.log("Deleting bill");
                return $http.post('/api/bill/delete', bill ).then(
                    function handleSuccess(res) {
                        $scope.bills.splice(idx, 1);
                        $scope.search();
                        $scope.reset();

                    },
                    function handleError(error) {
                        toaster.pop('error',"","something wrong happened !! Please report to Admin");
                        $scope.reset();
                    });
            }
        };

        $scope.payBill = function(bill, idx) {
            $scope.selectedBill = angular.copy(bill);
            ngDialog.open({
                template: '/app/views/checkDetail/list.html',
                className: 'ngdialog-theme-default custom-width-600',
                controller:'CheckDetailCtrl',
                scope: $scope,
                closeByDocument: false,
                closeByEscape: true
            });          
        };        

        $scope.update = function (idx) {
            console.log("Saving bill");
            return $http.post('/api/bill/update', $scope.selected ).then(
                function handleSuccess(res) {
                    $scope.bills[idx] = angular.copy($scope.selected);
                    $scope.search();
                    $scope.reset();

                },
                function handleError(error) {
                    toaster.pop('error',"","something wrong happened !! Please report to Admin");
                    $scope.reset();
                });


        };

        $scope.reset = function () {
            $scope.selected = {};
        };


        var self = $scope;

// list of `state` value/display objects
        self.states        = loadAll();
        self.driverArray= loadAllActiveDrivers();
        self.selectedStateItem  = null;
        self.searchText    = null;
        self.querySearch   = querySearch;

        $scope.ok = function () {
            //$scope.company.state=this.selectedStateItem.value;
            return $http.post('/api/bill/save', $scope.bill).then(
                function handleSuccess(res) {
                    $scope.billStatus.push(res.data);
                    $scope.search();
                    toaster.pop('success',"","New Bill Created");
                    $scope.billArray.push(res.data);
                },
                function handleError(error) {
                    toaster.pop('error',"","something wronng happened !!");
                });
        };
        $scope.addBill = function() {
            ngDialog.open({
                template: '/app/views/bill/add.html',
                className: 'ngdialog-theme-default custom-width-600',
                width:600,
                scope: $scope,
                closeByDocument: false,
                closeByEscape: false
            });
        };

        $scope.runReport = function () {
            var item = document.getElementById('txtDriverName').value;
            var driver = document.querySelector("#drivers option[value='"+ item +"']");

            if(driver) {
                driverId = driver.dataset.id;
                $scope.payment.driverId = driverId;
                $http.get('api/bills/driver/'+$scope.payment.driverId, {
                    params: { 
                        startDate: $scope.payment.startDate.toISOString().split('T')[0], 
                        endDate  : $scope.payment.endDate.toISOString().split('T')[0] 
                    }
                    }).then(
                    function  handleSuccess(res) {
                        //$scope.legsByDate = res.data;
                $scope.bills=res.data;
                return $scope.bills, $scope.searchKeywords = "", $scope.filteredList = [], $scope.row = "", $scope.select = function (page) {
                    var end, start;
                    return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageList = $scope.filteredList.slice(start, end);
                }, $scope.onFilterChange = function () {
                    return $scope.select(1), $scope.currentPage = 1, $scope.row = "";
                }, $scope.onNumPerPageChange = function () {
                    return $scope.select(1), $scope.currentPage = 1;
                }, $scope.onOrderChange = function () {
                    return $scope.select(1), $scope.currentPage = 1;
                }, $scope.search = function () {
                    return $scope.filteredList = $filter("filter")($scope.bills, $scope.searchKeywords), $scope.onFilterChange();
                }, $scope.order = function (rowName) {
                    return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredList = $filter("orderBy")($scope.bills, rowName), $scope.onOrderChange()) : void 0;
                }, $scope.numPerPageOpt = [3, 5, 10, 20], $scope.numPerPage = $scope.numPerPageOpt[2], $scope.currentPage = 1, $scope.currentPageList = [], (init = function () {
                    return $scope.search(), $scope.select($scope.currentPage);
                }), $scope.search();                        
                    }, function handleError (error) {
                        console.log(error);
                });
            } else {
                alert("Please select Driver");
            }
            
        };

        $http.get('/api/bills', $scope).then(
            function handleSuccess(res) {
                $scope.bills=res.data;
                return $scope.bills, $scope.searchKeywords = "", $scope.filteredList = [], $scope.row = "", $scope.select = function (page) {
                    var end, start;
                    return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageList = $scope.filteredList.slice(start, end);
                }, $scope.onFilterChange = function () {
                    return $scope.select(1), $scope.currentPage = 1, $scope.row = "";
                }, $scope.onNumPerPageChange = function () {
                    return $scope.select(1), $scope.currentPage = 1;
                }, $scope.onOrderChange = function () {
                    return $scope.select(1), $scope.currentPage = 1;
                }, $scope.search = function () {
                    return $scope.filteredList = $filter("filter")($scope.bills, $scope.searchKeywords), $scope.onFilterChange();
                }, $scope.order = function (rowName) {
                    return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredList = $filter("orderBy")($scope.bills, rowName), $scope.onOrderChange()) : void 0;
                }, $scope.numPerPageOpt = [3, 5, 10, 20], $scope.numPerPage = $scope.numPerPageOpt[2], $scope.currentPage = 1, $scope.currentPageList = [], (init = function () {
                    return $scope.search(), $scope.select($scope.currentPage);
                }), $scope.search();
            },
            function handleError(error) {
                return function () {
                };
            });



        // ******************************
        // Internal methods
        // ******************************

        function loadAllActiveDrivers() {
            $http.get('/api/driver/list', $scope).then(
                function handleSuccess(res) {
                    self.driverArray = res.data;
                },
                function handleError(error) {
                    return function () {
                    };
                });
        }

        /**
         * Search for states... use $timeout to simulate
         * remote dataservice call.
         */
        function querySearch (query) {
            var results = query ? self.states.filter( createFilterFor(query) ) : self.states;
            var deferred = $q.defer();
            $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
            return results;
        }

        /**
         * Build `states` list of key/value pairs
         */
        function loadAll() {
            var allStates = 'Alabama, Alaska, Arizona, Arkansas, California, Colorado, Connecticut, Delaware,Florida, Georgia, Hawaii, Idaho, Illinois, Indiana, Iowa, Kansas, Kentucky, Louisiana, Maine, Maryland, Massachusetts, Michigan, Minnesota, Mississippi, Missouri, Montana, Nebraska, Nevada, New Hampshire, New Jersey, New Mexico, New York, North Carolina,North Dakota, Ohio, Oklahoma, Oregon, Pennsylvania, Rhode Island, South Carolina,South Dakota, Tennessee, Texas, Utah, Vermont, Virginia, Washington, West Virginia,Wisconsin, Wyoming';

            return allStates.split(/, +/g).map( function (state) {
                return {
                    value: state.toLowerCase(),
                    display: state
                };
            });
        }

        /**
         * Create filter function for a query string
         */
        function createFilterFor(query) {
            var lowercaseQuery = angular.lowercase(query);

            return function filterFn(state) {
                return (state.value.indexOf(lowercaseQuery) === 0);
            };

        }
});
