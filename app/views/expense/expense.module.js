angular.module("app.expense",[])
    .config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('app.expense', {
            url: '/expense',
            abstract: true

        })

        .state('app.expense.all', {
            url: '/list',
            params:{
                mode:'list'
            },
            views : {
                "@": {
                    templateUrl: "app/views/expense/list.html",
                    controller:'ExpenseCtrl'
                }
            }

        });
});
