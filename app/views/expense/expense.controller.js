
angular.module("app.expense").controller("ExpenseCtrl",
    function ($scope,$http,$location,$filter,ngDialog,toaster) {
        var init;
        $scope.expenses=[];
        $scope.expense={};
        $scope.selected= {};
        $scope.expenseArray = [];
        // gets the template to ng-include for a table row / item
        $scope.getTemplate = function (expense) {
            if (expense.id === $scope.selected.id) return 'edit';
            else return 'display';
        };

        $scope.edit = function (expense) {
            $scope.selected = angular.copy(expense);
        };

        $scope.addExpense = function() {
            ngDialog.open({
                template: '/app/views/expense/add.html',
                className: 'ngdialog-theme-default custom-width-400',
                width:400,
                scope: $scope,
                closeByDocument: false,
                closeByEscape: false
            });
        };
        $scope.delete = function (expense,idx) {
            if (confirm("Do you want to delete ? Please confirm.")) {

            console.log("Deleting expense");
            return $http.post('/api/order/'+$scope.$parent.selectedOrder.orderId+'/expense/delete', expense ).then(
                function handleSuccess(res) {
                    $scope.expenses.splice(idx, 1);
                    $scope.search();
                },
                function handleError(error) {
                    toaster.pop('error',"","something wrong happened !! Please report to Admin");
                });
            }
        };

        $scope.addNewCharge = function() {
            $scope.orderCharges.push('');
        };
        
        $scope.update = function (idx) {
            console.log("Saving expense");
            return $http.post('/api/order/'+$scope.$parent.selectedOrder.orderId+'/expense/update', $scope.selected ).then(
                function handleSuccess(res) {
                    $scope.expenses[idx] = angular.copy($scope.selected);
                    $scope.search();
                    $scope.reset();

                },
                function handleError(error) {
                    toaster.pop('error',"","something wrong happened !! Please report to Admin");
                    $scope.reset();
                });
        };

        $scope.reset = function () {
            $scope.selected = {};
        };

        $scope.saveExpense = function() {
            var descriptions = document.getElementsByClassName('expense-name');
            var totalCharges = 0;
            $scope.dispatchOrder.extraCharges = 0;
            return angular.forEach($scope.orderCharges, function(charge, index){
                $scope.dispatchOrder.extraCharges += charge.amount;
            });
        };
        $scope.ok = function() {
             return $http.post('/api/order/'+$scope.$parent.selectedOrder.orderId+'/expense/save', $scope.expense).then(
                function handleSuccess(res) {
                    $scope.expenses.push(res.data);
                    $scope.search();
                    toaster.pop('success',"","New expense Created");
                    // $scope.expenseArray.push(res.data);
                    // $scope.dispatchOrder.selectedexpenseItem  = res.data;
                    // $scope.dispatchOrder.searchexpenseText    = $scope.expense.name;
                },
                function handleError(error) {
                    toaster.pop('error',"","something wrong happened !! Please report to Admin");
                });
        };

        $scope.getStates = function() {
            var data = ['AK', 'AL', 'AZ', 'AR', 'CA', 'CO', 'CT'];
            return data;
        };
        $http.get('/api/order/'+$scope.$parent.selectedOrder.orderId+'/expense', $scope).then(
            function handleSuccess(res) {
                $scope.expenses=res.data;
                return $scope.expenses, $scope.searchKeywords = "", $scope.filteredexpenses = [], $scope.row = "", $scope.select = function (page) {
                    var end, start;
                    return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageExpenses = $scope.filteredexpenses.slice(start, end);
                }, $scope.onFilterChange = function () {
                    return $scope.select(1), $scope.currentPage = 1, $scope.row = "";
                }, $scope.onNumPerPageChange = function () {
                    return $scope.select(1), $scope.currentPage = 1;
                }, $scope.onOrderChange = function () {
                    return $scope.select(1), $scope.currentPage = 1;
                }, $scope.search = function () {
                    return $scope.filteredexpenses = $filter("filter")($scope.expenses, $scope.searchKeywords), $scope.onFilterChange();
                }, $scope.order = function (rowName) {
                    return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredexpenses = $filter("orderBy")($scope.expenses, rowName), $scope.onOrderChange()) : void 0;
                }, $scope.numPerPageOpt = [3, 5, 10, 20], $scope.numPerPage = $scope.numPerPageOpt[2], $scope.currentPage = 1, $scope.currentPageExpenses = [], (init = function () {
                    return $scope.search(), $scope.select($scope.currentPage);
                }), $scope.search();
            },
            function handleError(error) {
                return function () {
                };
            });
    }
);




