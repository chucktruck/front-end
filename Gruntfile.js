/*jslint node: true */
"use strict";


module.exports = function(grunt) {
    require('load-grunt-tasks')(grunt);
    require('time-grunt')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        bower: {
            install: {
                options: {
                    install: true,
                    copy: false,
                    targetDir: './libs',
                    cleanTargetDir: true
                }
            }
        },

        copy: {
            main: {
                files: [{
                    //for jquery files
                    expand: true,
                    dot: true,
                    cwd: 'bower_components/jquery/dist',
                    src: ['jquery.min.js','jquery.min.map'],
                    dest: 'dist/js'
                },{
                    //for HTML5Shiv files
                    expand: true,
                    dot: true,
                    cwd: 'bower_components/html5shiv/dist',
                    src: ['html5shiv.min.js'],
                    dest: 'dist/js'
                },{
                    //for Fontawesome stylesheet files
                    expand: true,
                    dot: true,
                    cwd: 'bower_components/fontawesome/css',
                    src: ['font-awesome.min.css'],
                    dest: 'dist/css'
                },{
                    //for Bootstrap stylesheet files
                    expand: true,
                    dot: true,
                    cwd: 'bower_components/bootstrap/dist/css',
                    src: ['bootstrap.min.css'],
                    dest: 'dist/css'
                },{
                    //for Bootstrap theme stylesheet files
                    expand: true,
                    dot: true,
                    cwd: 'bower_components/bootstrap/dist/css',
                    src: ['bootstrap-theme.min.css'],
                    dest: 'dist/css'
                },{
                    //for bootstrap fonts
                    expand: true,
                    dot: true,
                    cwd: 'bower_components/bootstrap/dist',
                    src: ['fonts/*.*'],
                    dest: 'dist'
                },{
                    //for font-awesome
                    expand: true,
                    dot: true,
                    cwd: 'bower_components/font-awesome',
                    src: ['fonts/*.*'],
                    dest: 'dist'
                },{
                    //for font-awesome
                    expand: true,
                    dot: true,
                    cwd: 'fonts/',
                    src: ['fonts/*.*'],
                    dest: 'dist'
                },
                    {
                        //for Images
                        expand: true,
                        dot: true,
                        cwd: 'images',
                        src: ['*.*','background/*'],
                        dest: 'dist/images'
                    }]
            },
            copyicons: {
                files: [{
                    //for Images
                    expand: true,
                    dot: true,
                    cwd: 'bower_components/material-design-icons',
                    src: ['*/svg/production/*.svg'],
                    dest: 'dist/img/icons',
                    flatten: true
                }]
            }
        },

        // sass: {
        //      dist: {
        //          files: {
        //              'styles/teststyle.css' : 'styles/master.scss'
        //          }
        //      }
        //  },
       cssmin: {
            combine: {
                files: {
                        'dist/css/main.css': [
						'bower_components/ngDialog/css/ngDialog.css',
						'bower_components/ngDialog/css/ngDialog-theme-default.css',
                        'bower_components/AgnularJS-Toaster/toaster.min.css',
                        'css/normalize.css',
                        'css/bootstrap.css',
                        'node_modules/angular-bootstrap-datetimepicker/src/css/datetimepicker.css'                        
                    ]
                }
            },
            add_banner: {
                options: {
                    banner: '/* My minified admin css file */'
                },
                files: {
                    'dist/css/main.css': ['dist/css/main.css']
                }
            }
        },

        html2js: {
            dist: {
                src: [ 'app/views/*.html' ],
                dest: 'tmp/views.js'
            }
        },

        clean: {
            temp: {
                src: [ 'tmp' ]
            }
        },

        concat: {
           
            dist: {
                src: [
                    'js/jquery.min.js',
                    'bower_components/bootstrap/dist/js/bootstrap.min.js',
                    'bower_components/angular/angular.min.js',
                    'bower_components/angular-animate/angular-animate.min.js',
                    'bower_components/angular-messages/angular-messages.min.js',
                    'bower_components/angular-route/angular-route.min.js',
                    'bower_components/underscore/underscore-min.js',
                    'bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
                    'bower_components/AngularJS-toaster/toaster.min.js',
                    'bower_components/angular-ui-router/release/angular-ui-router.min.js',
                    'bower_components/ngDialog/js/ngDialog.min.js',
                    'node_modules/moment/moment.js',
                    'node_modules/angular-bootstrap-datetimepicker/src/js/datetimepicker.js',
                    'node_modules/angular-bootstrap-datetimepicker/src/js/datetimepicker.templates.js',
                    'node_modules/angular-bootstrap-datetimepicker/src/js/dateTimeInput.js',
                    'js/namespace.js',
                    'app/**/*module*.js',
                    'app/**/*services*.js',
                    'app/**/*.js'],
                dest: 'dist/js/app.js',
                
            },
        //     scss: {
        //        src: [ 'styles/*.scss' ],
        //        dest: 'styles/master.scss'
        //    } 
        },

        // concatscss: {
        //    scss: {
        //        src: [ 'styles/*.scss' ],
        //        dest: 'styles/master.scss'
        //    } 
        // },
        jshint: {
            all: [ 'Gruntfile.js', 'app/*.js', 'app/**/*.js' ]
        },

        connect: {
            proxies: [{
                context: '/api',
                port: 8080,
                //host: 'pure-meadow-29899.herokuapp.com',
                host: 'localhost',
                https: true,
                changeOrigin: false
                // headers : {
                //     'host': 'dispatchportal-802e6.firebaseapp.com'
                // }
            }
            ],

            options: {
                port: 9001,
                // Change this to 'localhost' to deny access to the server from outside.
                hostname: 'localhost',
                livereload: 35729,
                ws: true
            },

           livereload: {
                options: {
                    open: true,
                    middleware: function (connect, options, defaultMiddleware) {
                        var proxy = require('grunt-connect-proxy/lib/utils').proxyRequest;
                        return [
                            // Include the proxy first
                            proxy
                        ].concat(defaultMiddleware);
                    }
                }
            }


        },

        watch: {
            dev: {
                files: ['Gruntfile.js', 'app/*.js', '*.html', 'styles/*.scss'],
                tasks: [ 'jshint','html2js:dist','copy:main', 'concat:dist', 'clean:temp' ],
                //tasks: ['jshint', 'html2js:dist', 'copy:main', 'concat:dist', 'clean:temp', 'sass:dist'],
                options: {
                    atBegin: true
                }
            }
            /*,
            min: {
                files: ['Gruntfile.js', 'app/*.js', '*.html', 'styles/*.scss'],
                tasks: ['jshint', 'html2js:dist', 'copy:main', 'concat:dist', 'clean:temp', 'uglify:dist', 'cssmin'],
                options: {
                    atBegin: true
                }
            }*/
        }, 
        reload: {
                /**
                 * By default, we want the Live Reload to work for all tasks; this is
                 * overridden in some tasks (like this file) where browser resources are
                 * unaffected. It runs by default on port 35729, which your browser
                 * plugin should auto-detect.
                 */
                options: {
                    livereload: true
                },

                /**
                 * When the Gruntfile changes, we just want to lint it. In fact, when
                 * your Gruntfile changes, it will automatically be reloaded!
                 */
                gruntfile: {
                    files: 'Gruntfile.js',
                    options: {
                        livereload: false
                    }
                },

                /**
                 * When our JavaScript source files change, we want to run lint them and
                 * run our unit tests.
                 */
                jssrc: {
                    files: ['app/**/*module*.js', 'app/**/*.js', '!app/**/*.spec.js', '!app/assets/**/*.js'],
                    tasks: ['copy:main', 'concat:dist']
                },

                html: {
                    files: ['app/**/*.html'],
                    tasks: ['copy:main', 'concat:dist']
                }



            },

    });

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-html2js');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-bower-task');
    grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');    
    grunt.loadNpmTasks('grunt-connect-proxy');

    grunt.renameTask('watch', 'reload');
    grunt.registerTask('build', [
        'jshint', 'html2js:dist','cssmin', 'copy:main', 'concat:dist', 'concat:scss', 'clean:temp', 'sass:dist'
    ]);
   grunt.registerTask('dev', [ 'abc','configureProxies','connect:livereload']);
    grunt.registerTask('watch', ['build','configureProxies', 'connect:livereload','reload']);
    grunt.registerTask('serve', ['build','configureProxies', 'connect:livereload','reload']);
    grunt.registerTask('test', [ 'bower', 'jshint' ]);
    //grunt.registerTask('minified', [  'watch:min' ]);
    grunt.registerTask('copy-icons', [ 'copy:copyicons' ]);
};
